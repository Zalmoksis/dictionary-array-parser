<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Parser\ArrayParser;

interface ArrayNormalizer {
    function normalizeEntry(array $entry): array;
    function normalizeSense(array $sense): array;
    function normalizeCollocation(array $collocation): array;
}
