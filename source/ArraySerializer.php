<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Parser\ArrayParser;

use Zalmoksis\Dictionary\Model\{Collocation, Entry, Sense};

interface ArraySerializer {
    function serializeEntry(Entry $entry): array;
    function serializeSense(Sense $sense): array;
    function serializeCollocation(Collocation $collocation): array;
}
