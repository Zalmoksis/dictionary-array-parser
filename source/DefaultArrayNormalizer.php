<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Parser\ArrayParser;

use Zalmoksis\Dictionary\Model\Collections\{
    Antonyms,
    Categories,
    Cognates,
    Collocations,
    Derivatives,
    Domains,
    Etymons,
    FormNodes,
    Headwords,
    Pronunciations,
    Registers,
    Senses,
    SoundChanges,
    Synonyms,
    Translations,
    Varieties,
};
use Zalmoksis\Dictionary\Model\{
    Antonym,
    Category,
    Cognate,
    Collocation,
    Context,
    Definition,
    Derivative,
    Domain,
    Entry,
    Etymon,
    Form,
    Gloss,
    Headword,
    HomographIndex,
    Language,
    Lemma,
    Pronunciation,
    Register,
    Sense,
    SoundChange,
    Synonym,
    Translation,
    Variety,
};
use Zalmoksis\Dictionary\Parser\ArrayParser\Exceptions\HomographIsNotPositiveInteger;

final class DefaultArrayNormalizer implements ArrayNormalizer {
    private DefaultArrayStructure $structure;
    private array $aliases = [];
    private array $normalizers = [];
    private array $subnormalizers = [];

    function __construct(DefaultArrayStructure $arrayStructure) {
        $this->structure = $arrayStructure;
        $this->aliases = [
            Senses::NODE_COLLECTION_NAME => [
                Senses::NODE_COLLECTION_NAME,
                Sense::NODE_NAME,
                's',
            ],
            Collocations::NODE_COLLECTION_NAME => [
                Collocations::NODE_COLLECTION_NAME,
                Collocation::NODE_NAME,
                'colloc',
                'col',
                'cl',
            ],
            Headwords::NODE_COLLECTION_NAME => [
                Headwords::NODE_COLLECTION_NAME,
                Headword::NODE_NAME,
                'head',
                'hw',
                'h',
            ],
            DefaultArrayStructure::HOMOGRAPH_LABEL => [
                DefaultArrayStructure::HOMOGRAPH_LABEL,
                'hom',
                'hg',
            ],
            Pronunciations::NODE_COLLECTION_NAME => [
                Pronunciations::NODE_COLLECTION_NAME,
                Pronunciation::NODE_NAME,
                'pron',
                'p',
            ],
            DefaultArrayStructure::FORMS_LABEL => [
                DefaultArrayStructure::FORMS_LABEL,
                Form::NODE_NAME,
                'f',
            ],
            Categories::NODE_COLLECTION_NAME => [
                Categories::NODE_COLLECTION_NAME,
                Category::NODE_NAME,
                'cat',
                'c',
            ],
            Varieties::NODE_COLLECTION_NAME => [
                Varieties::NODE_COLLECTION_NAME,
                Variety::NODE_NAME,
                'var',
            ],
            Registers::NODE_COLLECTION_NAME => [
                Registers::NODE_COLLECTION_NAME,
                Register::NODE_NAME,
                'reg',
            ],
            Domains::NODE_COLLECTION_NAME => [
                Domains::NODE_COLLECTION_NAME,
                Domain::NODE_NAME,
                'dom',
            ],
            Context::NODE_NAME => [
                Context::NODE_NAME,
                'cont',
                'ct',
            ],
            Definition::NODE_NAME => [
                Definition::NODE_NAME,
                'def',
                'd',
            ],
            Translations::NODE_COLLECTION_NAME => [
                Translations::NODE_COLLECTION_NAME,
                Translation::NODE_NAME,
                'transl',
                'trans',
                'tr',
                't',
            ],
            Synonyms::NODE_COLLECTION_NAME => [
                Synonyms::NODE_COLLECTION_NAME,
                Synonym::NODE_NAME,
                'syn',
            ],
            Antonyms::NODE_COLLECTION_NAME => [
                Antonyms::NODE_COLLECTION_NAME,
                Antonym::NODE_NAME,
                'ant',
            ],
            Derivatives::NODE_COLLECTION_NAME => [
                Derivatives::NODE_COLLECTION_NAME,
                Derivative::NODE_NAME,
                'deriv',
                'der',
            ],
            Etymons::NODE_COLLECTION_NAME => [
                Etymons::NODE_COLLECTION_NAME,
                Etymon::NODE_NAME,
                'etym',
                'et',
            ],
            $this->structure->getArrayKeyForNodeName(SoundChanges::NODE_COLLECTION_NAME) => [
                $this->structure->getArrayKeyForNodeName(SoundChanges::NODE_COLLECTION_NAME),
                $this->structure->getArrayKeyForNodeName(SoundChange::NODE_NAME),
                'change',
                'chan',
                'sch',
                'sc',
            ],
            Cognates::NODE_COLLECTION_NAME => [
                Cognates::NODE_COLLECTION_NAME,
                Cognate::NODE_NAME,
                'cogn',
                'cgn',
                'cg',
            ],
            Language::NODE_NAME => [
                Language::NODE_NAME,
                'lang',
                'lng',
                'ln',
            ],
            Lemma::NODE_NAME => [
                Lemma::NODE_NAME,
                'lem',
                'lm',
            ],
            Gloss::NODE_NAME => [
                Gloss::NODE_NAME,
                'gl',
                'g'
            ],
        ];

        $this->normalizers = [
            Entry::NODE_NAME                       => [$this, 'normalizeEntry'],
            Sense::NODE_NAME                       => [$this, 'normalizeSense'],
            Collocation::NODE_NAME                 => [$this, 'normalizeCollocation'],
            Headword::NODE_NAME                    => [$this, 'normalizeStringValue'],
            $this->structure->getArrayKeyForNodeName(HomographIndex::NODE_NAME) => [$this, 'normalizeIntValue'],
            Pronunciation::NODE_NAME               => [$this, 'normalizeStringValue'],
            Category::NODE_NAME                    => [$this, 'normalizeStringValue'],
            Variety::NODE_NAME                     => [$this, 'normalizeStringValue'],
            Register::NODE_NAME                    => [$this, 'normalizeStringValue'],
            Domain::NODE_NAME                      => [$this, 'normalizeStringValue'],
            Context::NODE_NAME                     => [$this, 'normalizeStringValue'],
            Definition::NODE_NAME                  => [$this, 'normalizeStringValue'],
            Translation::NODE_NAME                 => [$this, 'normalizeStringValue'],
            Synonym::NODE_NAME                     => [$this, 'normalizeStringValue'],
            Antonym::NODE_NAME                     => [$this, 'normalizeStringValue'],
            Derivative::NODE_NAME                  => [$this, 'normalizeStringValue'],
            Etymon::NODE_NAME                      => [$this, 'normalizeEtymon'],
            $this->structure->getArrayKeyForNodeName(SoundChange::NODE_NAME) => [$this, 'normalizeStringValue'],
            Cognate::NODE_NAME                     => [$this, 'normalizeCognate'],
            Language::NODE_NAME                    => [$this, 'normalizeStringValue'],
            Lemma::NODE_NAME                       => [$this, 'normalizeStringValue'],
            Gloss::NODE_NAME                       => [$this, 'normalizeStringValue'],
        ];

        $this->subnormalizers = [
            Senses::NODE_COLLECTION_NAME           => [$this, 'normalizeAllMatchingCollectionAliasesInNode'],
            Collocations::NODE_COLLECTION_NAME     => [$this, 'normalizeAllMatchingCollectionAliasesInNode'],
            Headwords::NODE_COLLECTION_NAME        => [$this, 'normalizeAllMatchingCollectionAliasesInNode'],
            $this->structure->getArrayKeyForNodeName(HomographIndex::NODE_NAME)
                => [$this, 'normalizeHomographIndex'],
            Pronunciations::NODE_COLLECTION_NAME   => [$this, 'normalizeAllMatchingCollectionAliasesInNode'],
            Categories::NODE_COLLECTION_NAME       => [$this, 'normalizeAllMatchingCollectionAliasesInNode'],
            $this->structure->getArrayKeyForNodeName(FormNodes::NODE_COLLECTION_NAME)
                => [$this, 'normalizeFormsInNode'],
            Context::NODE_NAME                     => [$this, 'normalizeFirstMatchingValueAliasInNode'],
            Varieties::NODE_COLLECTION_NAME        => [$this, 'normalizeAllMatchingCollectionAliasesInNode'],
            Registers::NODE_COLLECTION_NAME        => [$this, 'normalizeAllMatchingCollectionAliasesInNode'],
            Domains::NODE_COLLECTION_NAME          => [$this, 'normalizeAllMatchingCollectionAliasesInNode'],
            Definition::NODE_NAME                  => [$this, 'normalizeFirstMatchingValueAliasInNode'],
            Translations::NODE_COLLECTION_NAME     => [$this, 'normalizeAllMatchingCollectionAliasesInNode'],
            Synonyms::NODE_COLLECTION_NAME         => [$this, 'normalizeAllMatchingCollectionAliasesInNode'],
            Antonyms::NODE_COLLECTION_NAME         => [$this, 'normalizeAllMatchingCollectionAliasesInNode'],
            Derivatives::NODE_COLLECTION_NAME      => [$this, 'normalizeAllMatchingCollectionAliasesInNode'],
            Etymons::NODE_COLLECTION_NAME          => [$this, 'normalizeAllMatchingCollectionAliasesInNode'],
            $this->structure->getArrayKeyForNodeName(SoundChanges::NODE_COLLECTION_NAME)
                => [$this, 'normalizeAllMatchingCollectionAliasesInNode'],
            Cognates::NODE_COLLECTION_NAME         => [$this, 'normalizeAllMatchingCollectionAliasesInNode'],
            Language::NODE_NAME                    => [$this, 'normalizeFirstMatchingValueAliasInNode'],
            Lemma::NODE_NAME                       => [$this, 'normalizeFirstMatchingValueAliasInNode'],
            Gloss::NODE_NAME                       => [$this, 'normalizeFirstMatchingValueAliasInNode'],
        ];
    }

    function normalizeEntry(array $entry): array {
        return $this->normalizeNode(Entry::NODE_NAME, $entry);
    }

    function normalizeSense(array $sense): array {
        return $this->normalizeNode(Sense::NODE_NAME, $sense);
    }

    function normalizeCollocation(array $collocation): array {
        return $this->normalizeNode(Collocation::NODE_NAME, $collocation);
    }

    private function normalizeHomographIndex(string $nodeName, array $nodeWithHomograph): int {
        return $this->validateHomographIndex(
            $this->normalizeFirstMatchingValueAliasInNode(
                $nodeName,
                $nodeWithHomograph,
            )
        );
    }

    private function normalizeFormsInNode(string $nodeName, array $nodeWithForms): array {
        return $this->normalizeAllMatchingCollectionAliasesInNodePreservingKeys($nodeName, $nodeWithForms);
    }

    private function normalizeEtymon(array $etymon): array {
        return $this->normalizeNode(Etymon::NODE_NAME, $etymon);
    }

    private function normalizeCognate(array $cognate): array {
        return $this->normalizeNode(Cognate::NODE_NAME, $cognate);
    }

    private function normalizeNode(string $nodeName, array $node): array {
        $normalizedNode = [];

        foreach ($this->structure->getChildrenForNodeName($nodeName) as $child) {
            $childKey = $this->structure->getArrayKeyForNodeName($child);

            if ($normalizedChild = $this->subnormalizers[$childKey]($child, $node)) {
                $normalizedNode[$childKey] = $normalizedChild;
            }
        }

        return $normalizedNode;
    }

    private function normalizeStringValue(string $value): string {
        return $value;
    }

    private function normalizeIntValue(int $value): int {
        return $value;
    }

    // returns elements from all aliases
    // TODO: Who needs such behavior?
    private function normalizeAllMatchingCollectionAliasesInNode(
        string $collectionName,
        array $nodeWithCollection
    ): array {
        $collection = [];

        foreach ($this->aliases[$this->structure->getArrayKeyForNodeName($collectionName)] as $collectionAlias) {
            if (array_key_exists($collectionAlias, $nodeWithCollection)) {
                $elementKey = $this->structure->getArrayKeyForNodeName(
                    $this->structure->getElementForCollectionName($collectionName)
                );

                foreach ((array)$nodeWithCollection[$collectionAlias] as $element) {
                    $collection[] = $this->normalizers[$elementKey]($element);
                }
            }
        }

        return $collection;
    }

    private function normalizeAllMatchingCollectionAliasesInNodePreservingKeys(
        string $collectionName,
        array $nodeWithCollection
    ): array {
        $collection = [];

        foreach ($this->aliases[$this->structure->getArrayKeyForNodeName($collectionName)] as $collectionAlias) {
            if (array_key_exists($collectionAlias, $nodeWithCollection)) {
                foreach ($nodeWithCollection[$collectionAlias] as $label => $element) {
                    $collection[$label] = $element;
                }
            }
        }

        return $collection;
    }

    // returns value of first matching alias
    // TODO: exception for further aliases?
    private function normalizeFirstMatchingValueAliasInNode(string $valueName, array $nodeWithValue) /* scalar */ {
        foreach ($this->aliases[$this->structure->getArrayKeyForNodeName($valueName)] as $valueAlias) {
            if (array_key_exists($valueAlias, $nodeWithValue)) {
                return $this->normalizers[$this->structure->getArrayKeyForNodeName($valueName)](
                    $nodeWithValue[$valueAlias]
                );
            }
        }

        return null;
    }

    private function validateHomographIndex($value): int {
        if (null === $value) {
            return 0;
        }

        $value = (int)$value;

        if ($value <= 0) {
            throw new HomographIsNotPositiveInteger();
        }

        return $value;
    }
}
