<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Parser\ArrayParser\Exceptions;

use RuntimeException;

class HomographIsNotPositiveInteger extends RuntimeException {
    protected $message = 'Homograph index is not a non-negative integer.';
}
