<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Parser\ArrayParser;

class DefaultArraySerializerConfiguration {
    protected array $allowedShorthands = [];

    function setAllowedShorthands(string ...$allowedShorthands): self {
        $this->allowedShorthands = $allowedShorthands;

        return $this;
    }

    function allowAllShorthands(): self {
        $this->allowedShorthands = ['*'];

        return $this;
    }

    function getAllowedShorthands(): array {
        return $this->allowedShorthands;
    }

    function isShorthandAllowed(string $key): bool {
        return in_array('*', $this->allowedShorthands)
            || in_array($key, $this->allowedShorthands);
    }
}
