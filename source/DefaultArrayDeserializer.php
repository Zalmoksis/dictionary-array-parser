<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Parser\ArrayParser;

use Zalmoksis\Dictionary\Model\{
    Antonym,
    Category,
    Cognate,
    Collocation,
    Context,
    Definition,
    Derivative,
    Domain,
    Entry,
    Etymon,
    Form,
    FormGroup,
    FormLabel,
    Gloss,
    Headword,
    HomographIndex,
    Language,
    Lemma,
    Node,
    Pronunciation,
    Register,
    Sense,
    SoundChange,
    Synonym,
    Translation,
    Variety,
};
use Zalmoksis\Dictionary\Model\Collections\{
    Antonyms,
    Categories,
    Cognates,
    Collocations,
    Derivatives,
    Domains,
    Etymons,
    FormNodes,
    Headwords,
    Pronunciations,
    Registers,
    Senses,
    SoundChanges,
    Synonyms,
    Translations,
    Varieties,
};
use Zalmoksis\Dictionary\Model\Interfaces\{
    NodeWithAntonyms,
    NodeWithCategories,
    NodeWithCognates,
    NodeWithCollocations,
    NodeWithContext,
    NodeWithDefinition,
    NodeWithDomains,
    NodeWithEtymons,
    NodeWithDerivatives,
    NodeWithFormNodes,
    NodeWithHeadwords,
    NodeWithHomographIndex,
    NodeWithPronunciations,
    NodeWithRegisters,
    NodeWithSenses,
    NodeWithSoundChanges,
    NodeWithSynonyms,
    NodeWithTranslations,
    NodeWithVarieties,
};
use Zalmoksis\Dictionary\Parser\ArrayParser\Exceptions\HomographIsNotPositiveInteger;

final class DefaultArrayDeserializer implements ArrayDeserializer {
    private DefaultArrayStructure $structure;

    private const DESERIALIZERS_IN_NODE = [
        Headwords::NODE_COLLECTION_NAME      => 'deserializeHeadwordsInNode',
        HomographIndex::NODE_NAME            => 'deserializeHomographIndexInNode',
        Pronunciations::NODE_COLLECTION_NAME => 'deserializePronunciationInNode',
        Categories::NODE_COLLECTION_NAME     => 'deserializeCategoriesInNode',
        FormNodes::NODE_COLLECTION_NAME      => 'deserializeFormNodesInNode',
        Varieties::NODE_COLLECTION_NAME      => 'deserializeVarietiesInNode',
        Registers::NODE_COLLECTION_NAME      => 'deserializeRegistersInNode',
        Domains::NODE_COLLECTION_NAME        => 'deserializeDomainsInNode',
        Context::NODE_NAME                   => 'deserializeContextInNode',
        Definition::NODE_NAME                => 'deserializeDefinitionInNode',
        Translations::NODE_COLLECTION_NAME   => 'deserializeTranslationsInNode',
        Synonyms::NODE_COLLECTION_NAME       => 'deserializeSynonymsInNode',
        Antonyms::NODE_COLLECTION_NAME       => 'deserializeAntonymsInNode',
        Collocations::NODE_COLLECTION_NAME   => 'deserializeCollocationsInNode',
        Senses::NODE_COLLECTION_NAME         => 'deserializeSensesInNode',
        Derivatives::NODE_COLLECTION_NAME    => 'deserializeDerivativesInNode',
        Etymons::NODE_COLLECTION_NAME        => 'deserializeEtymonsInNode',
        SoundChanges::NODE_COLLECTION_NAME   => 'deserializeSoundChangesInNode',
        Cognates::NODE_COLLECTION_NAME       => 'deserializeCognatesInNode',
    ];

    private const COLLECTION_TO_SETTER = [
        Senses::NODE_COLLECTION_NAME         => 'setSenses',
        Collocations::NODE_COLLECTION_NAME   => 'setCollocations',
        Headwords::NODE_COLLECTION_NAME      => 'setHeadwords',
        Pronunciations::NODE_COLLECTION_NAME => 'setPronunciations',
        Categories::NODE_COLLECTION_NAME     => 'setCategories',
        FormNodes::NODE_COLLECTION_NAME      => 'setFormNodes',
        Varieties::NODE_COLLECTION_NAME      => 'setVarieties',
        Registers::NODE_COLLECTION_NAME      => 'setRegisters',
        Domains::NODE_COLLECTION_NAME        => 'setDomains',
        Translations::NODE_COLLECTION_NAME   => 'setTranslations',
        Synonyms::NODE_COLLECTION_NAME       => 'setSynonyms',
        Antonyms::NODE_COLLECTION_NAME       => 'setAntonyms',
        Derivatives::NODE_COLLECTION_NAME    => 'setDerivatives',
        Etymons::NODE_COLLECTION_NAME        => 'setEtymons',
        SoundChanges::NODE_COLLECTION_NAME   => 'setSoundChanges',
        Cognates::NODE_COLLECTION_NAME       => 'setCognates',
    ];

    private const COLLECTION_TO_DESERIALIZER = [
        Senses::NODE_COLLECTION_NAME         => 'deserializeSenses',
        Collocations::NODE_COLLECTION_NAME   => 'deserializeCollocations',
        Headwords::NODE_COLLECTION_NAME      => 'deserializeHeadwords',
        Pronunciations::NODE_COLLECTION_NAME => 'deserializePronunciations',
        Categories::NODE_COLLECTION_NAME     => 'deserializeCategories',
        FormNodes::NODE_COLLECTION_NAME      => 'deserializeFormNodes',
        Varieties::NODE_COLLECTION_NAME      => 'deserializeVarieties',
        Registers::NODE_COLLECTION_NAME      => 'deserializeRegisters',
        Domains::NODE_COLLECTION_NAME        => 'deserializeDomains',
        Translations::NODE_COLLECTION_NAME   => 'deserializeTranslations',
        Synonyms::NODE_COLLECTION_NAME       => 'deserializeSynonyms',
        Antonyms::NODE_COLLECTION_NAME       => 'deserializeAntonyms',
        Derivatives::NODE_COLLECTION_NAME    => 'deserializeDerivatives',
        Etymons::NODE_COLLECTION_NAME        => 'deserializeEtymons',
        SoundChanges::NODE_COLLECTION_NAME   => 'deserializeSoundChanges',
        Cognates::NODE_COLLECTION_NAME       => 'deserializeCognates',
    ];

    function __construct(DefaultArrayStructure $defaultArrayStructure) {
        $this->structure = $defaultArrayStructure;
    }

    function deserializeEntry(array $entryArray): Entry {
        $entry = new Entry();

        $this->deserializeChildrenInNode($entryArray, $entry);

        return $entry;
    }

    private function deserializeSenses(array $sensesArray): Senses {
        return new Senses(...array_map([$this, 'deserializeSense'], $sensesArray));
    }

    function deserializeSense(array $senseArray): Sense {
        $sense = new Sense();

        $this->deserializeChildrenInNode($senseArray, $sense);

        return $sense;
    }

    function deserializeCollocations(array $collocationsArray): Collocations {
        return new Collocations(...array_map([$this, 'deserializeCollocation'], $collocationsArray));
    }

    function deserializeCollocation(array $collocationArray): Collocation {
        $collocation = new Collocation();

        $this->deserializeChildrenInNode($collocationArray, $collocation);

        return $collocation;
    }

    function deserializeHeadwords(array $headwordsArray): Headwords {
        return new Headwords(...array_map([$this, 'deserializeHeadword'], $headwordsArray));
    }

    function deserializeHeadword(string $headwordString): Headword {
        return new Headword($headwordString);
    }

    function deserializePronunciations(array $pronunciationsArray): Pronunciations {
        return new Pronunciations(...array_map([$this, 'deserializePronunciation'], $pronunciationsArray));
    }

    function deserializePronunciation(string $pronunciationString): Pronunciation {
        return new Pronunciation($pronunciationString);
    }

    function deserializeCategories(array $categoriesArray): Categories {
        return new Categories(...array_map([$this, 'deserializeCategory'], $categoriesArray));
    }

    function deserializeCategory(string $categoryString): Category {
        return new Category($categoryString);
    }

    function deserializeFormNodes(array $formsArray): FormNodes {
        $formNodes = [];

        foreach ($formsArray as $formLabel => $formNode) {
            $formNode = (array) $formNode;

            if ($this->isSequential($formNode)) {
                $formNodes[] = (new Form())
                    ->setFormLabel(new FormLabel($formLabel))
                    ->setHeadwords($this->deserializeHeadwords($formNode))
                ;
            } else {
                $formNodes[] = (new FormGroup())
                    ->setFormLabel(new FormLabel($formLabel))
                    ->setFormNodes($this->deserializeFormNodes($formNode))
                ;
            }
        }

        return new FormNodes(...$formNodes);
    }

    function deserializeVarieties(array $varietiesArray): Varieties {
        return new Varieties(...array_map([$this, 'deserializeVariety'], $varietiesArray));
    }

    function deserializeVariety(string $varietyString): Variety {
        return new Variety($varietyString);
    }

    function deserializeRegisters(array $registersArray): Registers {
        return new Registers(...array_map([$this, 'deserializeRegister'], $registersArray));
    }

    function deserializeRegister(string $registerString): Register {
        return new Register($registerString);
    }

    function deserializeDomains(array $domainsArray): Domains {
        return new Domains(...array_map([$this, 'deserializeDomain'], $domainsArray));
    }

    function deserializeDomain(string $domainString): Domain {
        return new Domain($domainString);
    }

    function deserializeContext(string $contextString): Context {
        return new Context($contextString);
    }

    function deserializeDefinition(string $definitionString): Definition {
        return new Definition($definitionString);
    }

    function deserializeTranslations(array $translationsArray): Translations {
        return new Translations(...array_map([$this, 'deserializeTranslation'], $translationsArray));
    }

    function deserializeTranslation(string $translationString): Translation {
        return new Translation($translationString);
    }

    function deserializeSynonyms(array $synonymsArray): Synonyms {
        return new Synonyms(...array_map([$this, 'deserializeSynonym'], $synonymsArray));
    }

    function deserializeSynonym(string $synonymString): Synonym {
        return new Synonym($synonymString);
    }

    function deserializeAntonyms(array $antonymsArray): Antonyms {
        return new Antonyms(...array_map([$this,  'deserializeAntonym'], $antonymsArray));
    }

    function deserializeAntonym(string $antonymString): Antonym {
        return new Antonym($antonymString);
    }

    function deserializeDerivatives(array $derivativesArray): Derivatives {
        return new Derivatives(...array_map([$this, 'deserializeDerivative'], $derivativesArray));
    }

    function deserializeDerivative($derivativeString): Derivative {
        return new Derivative($derivativeString);
    }

    function deserializeEtymons(array $etymonsArray): Etymons {
        return new Etymons(...array_map([$this, 'deserializeEtymon'], $etymonsArray));
    }

    function deserializeEtymon(array $etymonArray): Etymon {
        $etymon = new Etymon();

        if (array_key_exists(Language::NODE_NAME, $etymonArray)) {
            $etymon->setLanguage($this->deserializeLanguage($etymonArray[Language::NODE_NAME]));
        }

        if (array_key_exists(Lemma::NODE_NAME, $etymonArray)) {
            $etymon->setLemma($this->deserializeLemma($etymonArray[Lemma::NODE_NAME]));
        }

        if (array_key_exists(Gloss::NODE_NAME, $etymonArray)) {
            $etymon->setGloss($this->deserializeGloss($etymonArray[Gloss::NODE_NAME]));
        }

        return $etymon;
    }

    function deserializeSoundChanges(array $soundChangesArray): SoundChanges {
        return new SoundChanges(...array_map([$this, 'deserializeSoundChange'], $soundChangesArray));
    }

    function deserializeSoundChange(string $soundChangeString): SoundChange {
        return new SoundChange($soundChangeString);
    }

    function deserializeCognates(array $cognatesArray): Cognates {
        return new Cognates(...array_map([$this, 'deserializeCognate'], $cognatesArray));
    }

    function deserializeCognate(array $cognateArray): Cognate {
        $cognate =  new Cognate();

        if (array_key_exists(Language::NODE_NAME, $cognateArray)) {
            $cognate->setLanguage($this->deserializeLanguage($cognateArray[Language::NODE_NAME]));
        }

        if (array_key_exists(Lemma::NODE_NAME, $cognateArray)) {
            $cognate->setLemma($this->deserializeLemma($cognateArray[Lemma::NODE_NAME]));
        }

        if (array_key_exists(Gloss::NODE_NAME, $cognateArray)) {
            $cognate->setGloss($this->deserializeGloss($cognateArray[Gloss::NODE_NAME]));
        }

        return $cognate;
    }

    function deserializeLanguage(string $languageString): Language {
        return new Language($languageString);
    }

    function deserializeLemma(string $lemmaString): Lemma {
        return new Lemma($lemmaString);
    }

    function deserializeGloss(string $glossString): Gloss {
        return new Gloss($glossString);
    }

    private function deserializeChildrenInNode(array $nodeArray, Node $node): void {
        foreach ($this->structure->getChildrenForNodeName($node::NODE_NAME) as $childName) {
            $this->{self::DESERIALIZERS_IN_NODE[$childName]}($nodeArray, $node);
        }
    }

    private function deserializeSensesInNode(array $nodeArray, NodeWithSenses $nodeWithSenses): void {
        $this->deserializeCollectionInNode(
            $nodeArray,
            $nodeWithSenses,
            Senses::NODE_COLLECTION_NAME,
        );
    }

    private function deserializeCollocationsInNode(array $nodeArray, NodeWithCollocations $nodeWithCollocations): void {
        $this->deserializeCollectionInNode(
            $nodeArray,
            $nodeWithCollocations,
            Collocations::NODE_COLLECTION_NAME,
        );
    }

    private function deserializeHeadwordsInNode(array $nodeArray, NodeWithHeadwords $nodeWithHeadwords): void {
        $this->deserializeCollectionInNode(
            $nodeArray,
            $nodeWithHeadwords,
            Headwords::NODE_COLLECTION_NAME,
        );
    }

    private function deserializeHomographIndexInNode(
        array $nodeArray,
        NodeWithHomographIndex $nodeWithHomographIndex
    ): void {
        if (array_key_exists(DefaultArrayStructure::HOMOGRAPH_LABEL, $nodeArray)) {
            if (!$this->validateHomograph($nodeArray[DefaultArrayStructure::HOMOGRAPH_LABEL])) {
                throw new HomographIsNotPositiveInteger();
            }

            $nodeWithHomographIndex->setHomographIndex(
                new HomographIndex((int)$nodeArray[DefaultArrayStructure::HOMOGRAPH_LABEL])
            );
        }
    }

    private function deserializePronunciationInNode(
        array $nodeArray,
        NodeWithPronunciations $nodeWithPronunciations
    ): void {
        $this->deserializeCollectionInNode(
            $nodeArray,
            $nodeWithPronunciations,
            Pronunciations::NODE_COLLECTION_NAME,
        );
    }

    private function deserializeCategoriesInNode(array $nodeArray, NodeWithCategories $nodeWithCategories): void {
        $this->deserializeCollectionInNode(
            $nodeArray,
            $nodeWithCategories,
            Categories::NODE_COLLECTION_NAME,
        );
    }

    private function deserializeFormNodesInNode(array $nodeArray, NodeWithFormNodes $nodeWithFormNodes): void {
        $this->deserializeCollectionInNode(
            $nodeArray,
            $nodeWithFormNodes,
            FormNodes::NODE_COLLECTION_NAME,
        );
    }

    private function deserializeContextInNode(array $nodeArray, NodeWithContext $nodeWithContext): void {
        if (array_key_exists(Context::NODE_NAME, $nodeArray)) {
            $nodeWithContext->setContext(
                $this->deserializeContext($nodeArray[Context::NODE_NAME])
            );
        }
    }

    private function deserializeVarietiesInNode(array $nodeArray, NodeWithVarieties $nodeWithVarieties): void {
        $this->deserializeCollectionInNode(
            $nodeArray,
            $nodeWithVarieties,
            Varieties::NODE_COLLECTION_NAME,
        );
    }

    private function deserializeRegistersInNode(array $nodeArray, NodeWithRegisters $nodeWithRegisters): void {
        $this->deserializeCollectionInNode(
            $nodeArray,
            $nodeWithRegisters,
            Registers::NODE_COLLECTION_NAME,
        );
    }

    private function deserializeDomainsInNode(array $nodeArray, NodeWithDomains $nodeWithDomains): void {
        $this->deserializeCollectionInNode(
            $nodeArray,
            $nodeWithDomains,
            Domains::NODE_COLLECTION_NAME,
        );
    }

    private function deserializeDefinitionInNode(array $nodeArray, NodeWithDefinition $nodeWithDefinition): void {
        if (array_key_exists(Definition::NODE_NAME, $nodeArray)) {
            $nodeWithDefinition->setDefinition(
                $this->deserializeDefinition($nodeArray[Definition::NODE_NAME])
            );
        }
    }

    private function deserializeTranslationsInNode(array $nodeArray, NodeWithTranslations $nodeWithTranslations): void {
        $this->deserializeCollectionInNode(
            $nodeArray,
            $nodeWithTranslations,
            Translations::NODE_COLLECTION_NAME,
        );
    }

    private function deserializeSynonymsInNode(array $nodeArray, NodeWithSynonyms $nodeWithSynonyms): void {
        $this->deserializeCollectionInNode(
            $nodeArray,
            $nodeWithSynonyms,
            Synonyms::NODE_COLLECTION_NAME,
        );
    }

    private function deserializeAntonymsInNode(array $nodeArray, NodeWithAntonyms $nodeWithAntonyms): void {
        $this->deserializeCollectionInNode(
            $nodeArray,
            $nodeWithAntonyms,
            Antonyms::NODE_COLLECTION_NAME,
        );
    }

    private function deserializeDerivativesInNode(array $nodeArray, NodeWithDerivatives $nodeWithDerivatives): void {
        $this->deserializeCollectionInNode(
            $nodeArray,
            $nodeWithDerivatives,
            Derivatives::NODE_COLLECTION_NAME,
        );
    }

    private function deserializeEtymonsInNode(array $nodeArray, NodeWithEtymons $nodeWithEtymons): void {
        $this->deserializeCollectionInNode(
            $nodeArray,
            $nodeWithEtymons,
            Etymons::NODE_COLLECTION_NAME,
        );
    }

    private function deserializeSoundChangesInNode(array $nodeArray, NodeWithSoundChanges $nodeWithSoundChanges): void {
        $this->deserializeCollectionInNode(
            $nodeArray,
            $nodeWithSoundChanges,
            SoundChanges::NODE_COLLECTION_NAME,
        );
    }

    private function deserializeCognatesInNode(array $nodeArray, NodeWithCognates $nodeWithCognates): void {
        $this->deserializeCollectionInNode(
            $nodeArray,
            $nodeWithCognates,
            Cognates::NODE_COLLECTION_NAME,
        );
    }

    private function deserializeCollectionInNode(
        array $nodeArray,
        /* Node */ $nodeWithCollection,
        string $collectionName
    ): void {
        $arrayKey = $this->structure->getArrayKeyForNodeName($collectionName);

        if (array_key_exists($arrayKey, $nodeArray)) {
            $nodeWithCollection->{self::COLLECTION_TO_SETTER[$collectionName]}(
                $this->{self::COLLECTION_TO_DESERIALIZER[$collectionName]}((array)$nodeArray[$arrayKey])
            );
        }
    }

    private function isSequential(array $array): bool {
        for (reset($array); is_int(key($array)); next($array));

        return key($array) === null;
    }

    private function validateHomograph($homograph): bool {
        return $this->isPositiveInt($homograph)
            || $this->isPositiveIntString($homograph);
    }

    private function isPositiveInt($homograph): bool {
        return is_int($homograph) && $homograph > 0;
    }

    private function isPositiveIntString($homograph): bool {
        return is_string($homograph) && (int)$homograph > 0;
    }
}
