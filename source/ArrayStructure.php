<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Parser\ArrayParser;

use Zalmoksis\Dictionary\Model\Collections\Nodes;
use Zalmoksis\Dictionary\Model\Node;

interface ArrayStructure {
    function getChildrenForNodeName(string $nodeName): array;
    function getElementForCollectionName(string $collectionName): string;
    function getArrayKeyForNode(Node $node): string;
    function getArrayKeyForCollection(Nodes $node): string;
    function getArrayKeyForNodeName(string $nodeName): string;
}
