<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Parser\ArrayParser;

use Zalmoksis\Dictionary\Model\{Collocation, Entry, Sense};

interface ArrayDeserializer {
    function deserializeEntry(array $serializedEntry): Entry;
    function deserializeSense(array $serializedSense): Sense;
    function deserializeCollocation(array $serializedCollocation): Collocation;
}
