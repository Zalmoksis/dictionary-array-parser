<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Parser\ArrayParser;

use Zalmoksis\Dictionary\Model\{
    Antonym,
    Category,
    Cognate,
    Collocation,
    Context,
    Definition,
    Derivative,
    Domain,
    Entry,
    Etymon,
    Form,
    FormGroup,
    Gloss,
    Headword,
    HomographIndex,
    Language,
    Lemma,
    Node,
    Pronunciation,
    Reference,
    Register,
    Sense,
    SoundChange,
    Synonym,
    Translation,
    Variety,
};
use Zalmoksis\Dictionary\Model\Collections\{
    Antonyms,
    Categories,
    Cognates,
    Collocations,
    Derivatives,
    Domains,
    Etymons,
    FormNodes,
    Headwords,
    Nodes,
    Pronunciations,
    Registers,
    Senses,
    SoundChanges,
    Synonyms,
    Translations,
    Varieties,
};
use Zalmoksis\Dictionary\Model\Interfaces\{
    NodeWithAntonyms,
    NodeWithCategories,
    NodeWithCognates,
    NodeWithCollocations,
    NodeWithContext,
    NodeWithDefinition,
    NodeWithDerivatives,
    NodeWithDomains,
    NodeWithEtymons,
    NodeWithFormNodes,
    NodeWithHeadwords,
    NodeWithHomographIndex,
    NodeWithPronunciations,
    NodeWithRegisters,
    NodeWithSenses,
    NodeWithSoundChanges,
    NodeWithSynonyms,
    NodeWithTranslations,
    NodeWithVarieties,
};

final class DefaultArraySerializer implements ArraySerializer {
    private DefaultArrayStructure $structure;
    private ?DefaultArraySerializerConfiguration $configuration;

    private const SERIALIZERS_IN_NODE = [
        Headwords::NODE_COLLECTION_NAME      => 'serializeNodeWithHeadwords',
        HomographIndex::NODE_NAME            => 'serializeNodeWithHomographIndex',
        Pronunciations::NODE_COLLECTION_NAME => 'serializeNodeWithPronunciations',
        Categories::NODE_COLLECTION_NAME     => 'serializeNodeWithCategories',
        FormNodes::NODE_COLLECTION_NAME      => 'serializeNodeWithFormNodes',
        Varieties::NODE_COLLECTION_NAME      => 'serializeNodeWithVarieties',
        Registers::NODE_COLLECTION_NAME      => 'serializeNodeWithRegisters',
        Domains::NODE_COLLECTION_NAME        => 'serializeNodeWithDomains',
        Context::NODE_NAME                   => 'serializeNodeWithContext',
        Definition::NODE_NAME                => 'serializeNodeWithDefinition',
        Translations::NODE_COLLECTION_NAME   => 'serializeNodeWithTranslations',
        Synonyms::NODE_COLLECTION_NAME       => 'serializeNodeWithSynonyms',
        Antonyms::NODE_COLLECTION_NAME       => 'serializeNodeWithAntonyms',
        Collocations::NODE_COLLECTION_NAME   => 'serializeNodeWithCollocations',
        Senses::NODE_COLLECTION_NAME         => 'serializeNodeWithSenses',
        Derivatives::NODE_COLLECTION_NAME    => 'serializeNodeWithDerivatives',
        Etymons::NODE_COLLECTION_NAME        => 'serializeNodeWithEtymons',
        SoundChanges::NODE_COLLECTION_NAME   => 'serializeNodeWithSoundChanges',
        Cognates::NODE_COLLECTION_NAME       => 'serializeNodeWithCognates',
    ];

    function __construct(
        DefaultArrayStructure $structure,
        DefaultArraySerializerConfiguration $configuration = null
    ) {
        $this->structure = $structure;
        $this->configuration = $configuration ?: new DefaultArraySerializerConfiguration();
    }

    function serializeEntry(Entry $entry): array {
        return $this->serializeNode($entry);
    }

    function serializeSenses(Senses $senses): array {
        return $this->serializeCollection($senses, [$this, 'serializeSense']);
    }

    function serializeSense(Sense $sense): array {
        return $this->serializeNode($sense);
    }

    function serializeCollocations(Collocations $collocations): array {
        return $this->serializeCollection($collocations, [$this, 'serializeCollocation']);
    }

    function serializeCollocation(Collocation $collocation): array {
        return $this->serializeNode($collocation);
    }

    function serializeHeadwords(Headwords $headwords) /* array | string */ {
        return $this->serializeCollectionWithShorthands($headwords, [$this, 'serializeHeadword']);
    }

    function serializeHeadword(Headword $headword): string {
        return $headword->getValue();
    }

    function serializeHomographIndex(HomographIndex $homographIndex): int {
        return $homographIndex->getValue();
    }

    function serializePronunciations(Pronunciations $pronunciations) /* array | string */ {
        return $this->serializeCollectionWithShorthands($pronunciations, [$this, 'serializePronunciation']);
    }

    function serializePronunciation(Pronunciation $pronunciation): string {
        return $pronunciation->getValue();
    }

    function serializeCategories(Categories $categories) /* array | string */ {
        return $this->serializeCollectionWithShorthands($categories, [$this, 'serializeCategory']);
    }

    function serializeCategory(Category $category): string {
        return $category->getValue();
    }

    function serializeFormNodes(FormNodes $formNodes): array {
        $serializedFormNodes = [];

        foreach ($formNodes as $formNode) {
            switch (true) {
                case $formNode instanceof Form:
                    $serializedFormNodes[$formNode->getFormLabel()->getValue()] = $this->serializeForm($formNode);
                    break;
                case $formNode instanceof FormGroup:
                    $serializedFormNodes[$formNode->getFormLabel()->getValue()] = $this->serializeFormGroup($formNode);
                    break;
            }
        }

        return $serializedFormNodes;
    }

    function serializeFormGroup(FormGroup $formGroup): array {
        return $this->serializeFormNodes($formGroup->getFormNodes());
    }

    function serializeForm(Form $form) /* array | string */ {
        return $form->getHeadwords()
            ? $this->serializeFormHeadwords($form->getHeadwords())
            : [];
    }

    private function serializeFormHeadwords(Headwords $headwords) /* array | string */ {
        return $this->applyShorthands(
            DefaultArrayStructure::FORMS_LABEL,
            $this->serializeCollection($headwords, [$this, 'serializeHeadword'])
        );
    }

    function serializeVarieties(Varieties $varieties) /* array | string */ {
        return $this->serializeCollectionWithShorthands($varieties, [$this, 'serializeVariety']);
    }

    function serializeVariety(Variety $variety): string {
        return $variety->getValue();
    }

    function serializeRegisters(Registers $registers) /* array | string */ {
        return $this->serializeCollectionWithShorthands($registers, [$this, 'serializeRegister']);
    }

    function serializeRegister(Register $register): string {
        return $register->getValue();
    }

    function serializeDomains(Domains $domains) /* array | string */ {
        return $this->serializeCollectionWithShorthands($domains, [$this, 'serializeDomain']);
    }

    function serializeDomain(Domain $domain): string {
        return $domain->getValue();
    }

    function serializeContext(Context $context): string {
        return $context->getValue();
    }

    function serializeDefinition(Definition $definition): string {
        return $definition->getValue();
    }

    function serializeTranslations(Translations $translations) /* array | string */ {
        return $this->serializeCollectionWithShorthands($translations, [$this, 'serializeTranslation']);
    }

    function serializeTranslation(Translation $translation): string {
        return $translation->getValue();
    }

    function serializeSynonyms(Synonyms $synonyms) /* array | string */ {
        return $this->serializeCollectionWithShorthands($synonyms, [$this, 'serializeSynonym']);
    }

    function serializeSynonym(Synonym $synonym): string {
        return $this->serializeReference($synonym);
    }

    function serializeAntonyms(Antonyms $antonyms) /* array | string */ {
        return $this->serializeCollectionWithShorthands($antonyms, [$this, 'serializeAntonym']);
    }

    function serializeAntonym(Antonym $antonym): string {
        return $this->serializeReference($antonym);
    }

    function serializeDerivatives(Derivatives $derivatives) /* array | string */ {
        return $this->serializeCollectionWithShorthands($derivatives, [$this, 'serializeDerivative']);
    }

    function serializeDerivative(Derivative $derivative): string {
        return $this->serializeReference($derivative);
    }

    function serializeEtymons(Etymons $etymons): array {
        return $this->serializeCollection($etymons, [$this, 'serializeEtymon']);
    }

    function serializeEtymon(Etymon $etymon): array {
        $serializedCrossLanguageReference = [];

        if ($etymon->getLanguage()) {
            $serializedCrossLanguageReference += $this->serializeLanguageInNode($etymon->getLanguage());
        }

        if ($etymon->getLemma()) {
            $serializedCrossLanguageReference += $this->serializeLemmaInNode($etymon->getLemma());
        }

        if ($etymon->getGloss()) {
            $serializedCrossLanguageReference += $this->serializeGlossInNode($etymon->getGloss());
        }

        return $serializedCrossLanguageReference;
    }

    function serializeSoundChanges(SoundChanges $soundChanges) /* array | string */ {
        return $this->serializeCollectionWithShorthands($soundChanges, [$this, 'serializeSoundChange']);
    }

    function serializeSoundChange(SoundChange $soundChange): string {
        return $soundChange->getValue();
    }

    function serializeCognates(Cognates $cognates): array {
        return $this->serializeCollection($cognates, [$this, 'serializeCognate']);
    }

    function serializeCognate(Cognate $cognate): array {
        $serializedCrossLanguageReference = [];

        if ($cognate->getLanguage()) {
            $serializedCrossLanguageReference += $this->serializeLanguageInNode($cognate->getLanguage());
        }

        if ($cognate->getLemma()) {
            $serializedCrossLanguageReference += $this->serializeLemmaInNode($cognate->getLemma());
        }

        if ($cognate->getGloss()) {
            $serializedCrossLanguageReference += $this->serializeGlossInNode($cognate->getGloss());
        }

        return $serializedCrossLanguageReference;
    }

    function serializeLanguage(Language $language): string {
        return $language->getValue();
    }

    function serializeLemma(Lemma $lemma): string {
        return $lemma->getValue();
    }

    function serializeGloss(Gloss $gloss): string {
        return $gloss->getValue();
    }

    private function serializeNode(Node $node): array {
        $result = [];

        foreach ($this->structure->getChildrenForNodeName($node::NODE_NAME) as $childName) {
            $result += $this->{self::SERIALIZERS_IN_NODE[$childName]}($node);
        }

        return $result;
    }

    private function serializeNodeWithSenses(NodeWithSenses $nodeWithSenses): array {
        $senses = $nodeWithSenses->getSenses();

        return $senses
            ? [$this->structure->getArrayKeyForCollection($senses) => $this->serializeSenses($senses)]
            : [];
    }

    private function serializeNodeWithCollocations(NodeWithCollocations $nodeWithCollocations): array {
        $collocations = $nodeWithCollocations->getCollocations();

        return $collocations
            ? [$this->structure->getArrayKeyForCollection($collocations) => $this->serializeCollocations($collocations)]
            : [];
    }

    private function serializeNodeWithHeadwords(NodeWithHeadwords $nodeWithHeadwords): array {
        $headwords = $nodeWithHeadwords->getHeadwords();

        return $headwords
            ? [$this->structure->getArrayKeyForCollection($headwords) => $this->serializeHeadwords($headwords)]
            : [];
    }

    private function serializeNodeWithHomographIndex(NodeWithHomographIndex $nodeWithHomographIndex): array {
        $homographIndex = $nodeWithHomographIndex->getHomographIndex();

        return $homographIndex
            ? [$this->structure->getArrayKeyForNode($homographIndex) => $this->serializeHomographIndex($homographIndex)]
            : [];
    }

    private function serializeNodeWithPronunciations(NodeWithPronunciations $nodeWithPronunciations): array {
        $pronunciations = $nodeWithPronunciations->getPronunciations();

        return $pronunciations
            ? [$this->structure->getArrayKeyForCollection($pronunciations) => $this->serializePronunciations($pronunciations)]
            : [];
    }

    private function serializeNodeWithCategories(NodeWithCategories $nodeWithCategories): array {
        $categories = $nodeWithCategories->getCategories();

        return $categories
            ? [$this->structure->getArrayKeyForCollection($categories) => $this->serializeCategories($categories)]
            : [];
    }

    private function serializeNodeWithFormNodes(NodeWithFormNodes $nodeWithFormNodes): array {
        $formNodes = $nodeWithFormNodes->getFormNodes();

        return $formNodes
            ? [$this->structure->getArrayKeyForCollection($formNodes) => $this->serializeFormNodes($formNodes)]
            : [];
    }

    private function serializeNodeWithVarieties(NodeWithVarieties $nodeWithVarieties): array {
        $varieties = $nodeWithVarieties->getVarieties();

        return $varieties
            ? [$this->structure->getArrayKeyForCollection($varieties) => $this->serializeVarieties($varieties)]
            : [];
    }

    private function serializeNodeWithRegisters(NodeWithRegisters $nodeWithRegisters): array {
        $registers = $nodeWithRegisters->getRegisters();

        return $registers
            ? [$this->structure->getArrayKeyForCollection($registers) => $this->serializeRegisters($registers)]
            : [];
    }

    private function serializeNodeWithDomains(NodeWithDomains $nodeWithDomains): array {
        $domains = $nodeWithDomains->getDomains();

        return $domains
            ? [$this->structure->getArrayKeyForCollection($domains) => $this->serializeDomains($domains)]
            : [];
    }

    private function serializeNodeWithContext(NodeWithContext $nodeWithContext): array {
        $context = $nodeWithContext->getContext();

        return $context
            ? [$this->structure->getArrayKeyForNode($context) => $this->serializeContext($context)]
            : [];
    }

    private function serializeNodeWithDefinition(NodeWithDefinition $nodeWithDefinition): array {
        $definition = $nodeWithDefinition->getDefinition();

        return $definition
            ? [$this->structure->getArrayKeyForNode($definition) => $this->serializeDefinition($definition)]
            : [];
    }

    private function serializeNodeWithTranslations(NodeWithTranslations $nodeWithTranslations): array {
        $translations = $nodeWithTranslations->getTranslations();

        return $translations
            ? [$this->structure->getArrayKeyForCollection($translations) => $this->serializeTranslations($translations)]
            : [];
    }

    private function serializeNodeWithSynonyms(NodeWithSynonyms $nodeWithSynonyms): array {
        $synonyms = $nodeWithSynonyms->getSynonyms();

        return $synonyms
            ? [$this->structure->getArrayKeyForCollection($synonyms) => $this->serializeSynonyms($synonyms)]
            : [];
    }

    private function serializeNodeWithAntonyms(NodeWithAntonyms $nodeWithAntonyms): array {
        $antonyms = $nodeWithAntonyms->getAntonyms();

        return $antonyms
            ? [$this->structure->getArrayKeyForCollection($antonyms) => $this->serializeAntonyms($antonyms)]
            : [];
    }

    private function serializeNodeWithDerivatives(NodeWithDerivatives $nodeWithDerivatives): array {
        $derivatives = $nodeWithDerivatives->getDerivatives();

        return $derivatives
            ? [$this->structure->getArrayKeyForCollection($derivatives) => $this->serializeDerivatives($derivatives)]
            : [];
    }

    private function serializeNodeWithEtymons(NodeWithEtymons $nodeWithEtymons): array {
        $etymons = $nodeWithEtymons->getEtymons();

        return $etymons
            ? [$this->structure->getArrayKeyForCollection($etymons) => $this->serializeEtymons($etymons)]
            : [];
    }

    private function serializeNodeWithSoundChanges(NodeWithSoundChanges $nodeWithSoundChanges): array {
        $soundChanges = $nodeWithSoundChanges->getSoundChanges();

        return $soundChanges
            ? [$this->structure->getArrayKeyForCollection($soundChanges) => $this->serializeSoundChanges($soundChanges)]
            : [];
    }

    private function serializeNodeWithCognates(NodeWithCognates $nodeWithCognates): array {
        $cognates = $nodeWithCognates->getCognates();

        return $cognates
            ? [$this->structure->getArrayKeyForCollection($cognates) => $this->serializeCognates($cognates)]
            : [];
    }

    private function serializeLanguageInNode(Language $language): array {
        return [$this->structure->getArrayKeyForNode($language) => $this->serializeLanguage($language)];
    }

    private function serializeLemmaInNode(Lemma $lemma): array {
        return [$this->structure->getArrayKeyForNode($lemma) => $this->serializeLemma($lemma)];
    }

    private function serializeGlossInNode(Gloss $gloss): array {
        return [$this->structure->getArrayKeyForNode($gloss) => $this->serializeGloss($gloss)];
    }

    // generic serializers

    private function serializeCollectionWithShorthands(Nodes $nodes, callable $serializeNode) /* array | string */ {
        return $this->applyShorthands(
            $this->structure->getArrayKeyForCollection($nodes),
            $this->serializeCollection($nodes, $serializeNode)
        );
    }

    private function serializeCollection(Nodes $nodes, callable $serializeNode): array {
        $serializedNodes = [];

        foreach ($nodes as $node) {
            $serializedNodes[] = $serializeNode($node);
        }

        return $serializedNodes;
    }

    private function applyShorthands($key, array $result) /* array | string */ {
        if ((count($result) === 1)
            && $this->configuration->isShorthandAllowed($key)
        ) {
            return $result[0];
        } else {
            return $result;
        }
    }

    private function serializeReference(Reference $reference): string {
        return $reference->getHeadword();
    }
}
