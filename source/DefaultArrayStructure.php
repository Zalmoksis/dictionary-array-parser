<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Parser\ArrayParser;

use Zalmoksis\Dictionary\Model\{
    Antonym,
    Category,
    Cognate,
    Collocation,
    Context,
    Definition,
    Derivative,
    Domain,
    Entry,
    Etymon,
    Gloss,
    Headword,
    HomographIndex,
    Language,
    Lemma,
    Node,
    Pronunciation,
    Register,
    Sense,
    SoundChange,
    Synonym,
    Translation,
    Variety,
};
use Zalmoksis\Dictionary\Model\Collections\{
    Antonyms,
    Categories,
    Cognates,
    Collocations,
    Derivatives,
    Domains,
    Entries,
    Etymons,
    FormNodes,
    Headwords,
    Nodes,
    Pronunciations,
    Registers,
    Senses,
    SoundChanges,
    Synonyms,
    Translations,
    Varieties,
};

final class DefaultArrayStructure implements ArrayStructure {
    public const HOMOGRAPH_LABEL = 'homograph';
    public const FORMS_LABEL     = 'forms';

    private const CHILDREN = [
        Entry::NODE_NAME => [
            Headwords::NODE_COLLECTION_NAME,
            HomographIndex::NODE_NAME,
            Pronunciations::NODE_COLLECTION_NAME,
            Categories::NODE_COLLECTION_NAME,
            FormNodes::NODE_COLLECTION_NAME,
            Varieties::NODE_COLLECTION_NAME,
            Registers::NODE_COLLECTION_NAME,
            Domains::NODE_COLLECTION_NAME,
            Definition::NODE_NAME,
            Translations::NODE_COLLECTION_NAME,
            Synonyms::NODE_COLLECTION_NAME,
            Antonyms::NODE_COLLECTION_NAME,
            Collocations::NODE_COLLECTION_NAME,
            Senses::NODE_COLLECTION_NAME,
            Derivatives::NODE_COLLECTION_NAME,
            Etymons::NODE_COLLECTION_NAME,
            SoundChanges::NODE_COLLECTION_NAME,
            Cognates::NODE_COLLECTION_NAME,
        ],
        Sense::NODE_NAME => [
            Varieties::NODE_COLLECTION_NAME,
            Registers::NODE_COLLECTION_NAME,
            Domains::NODE_COLLECTION_NAME,
            Context::NODE_NAME,
            Definition::NODE_NAME,
            Translations::NODE_COLLECTION_NAME,
            Synonyms::NODE_COLLECTION_NAME,
            Antonyms::NODE_COLLECTION_NAME,
            Collocations::NODE_COLLECTION_NAME,
            Senses::NODE_COLLECTION_NAME,
        ],
        Collocation::NODE_NAME => [
            Headwords::NODE_COLLECTION_NAME,
            Pronunciations::NODE_COLLECTION_NAME,
            Varieties::NODE_COLLECTION_NAME,
            Registers::NODE_COLLECTION_NAME,
            Domains::NODE_COLLECTION_NAME,
            Definition::NODE_NAME,
            Translations::NODE_COLLECTION_NAME,
            Senses::NODE_COLLECTION_NAME,
        ],
        Etymon::NODE_NAME => [
            Language::NODE_NAME,
            Lemma::NODE_NAME,
            Gloss::NODE_NAME,
        ],
        Cognate::NODE_NAME => [
            Language::NODE_NAME,
            Lemma::NODE_NAME,
            Gloss::NODE_NAME,
        ]
    ];

    private const ELEMENTS = [
        Entries::NODE_COLLECTION_NAME        => Entry::NODE_NAME,
        Senses::NODE_COLLECTION_NAME         => Sense::NODE_NAME,
        Collocations::NODE_COLLECTION_NAME   => Collocation::NODE_NAME,
        Headwords::NODE_COLLECTION_NAME      => Headword::NODE_NAME,
        Pronunciations::NODE_COLLECTION_NAME => Pronunciation::NODE_NAME,
        Categories::NODE_COLLECTION_NAME     => Category::NODE_NAME,
        Varieties::NODE_COLLECTION_NAME      => Variety::NODE_NAME,
        Registers::NODE_COLLECTION_NAME      => Register::NODE_NAME,
        Domains::NODE_COLLECTION_NAME        => Domain::NODE_NAME,
        Translations::NODE_COLLECTION_NAME   => Translation::NODE_NAME,
        Synonyms::NODE_COLLECTION_NAME       => Synonym::NODE_NAME,
        Antonyms::NODE_COLLECTION_NAME       => Antonym::NODE_NAME,
        Derivatives::NODE_COLLECTION_NAME    => Derivative::NODE_NAME,
        Etymons::NODE_COLLECTION_NAME        => Etymon::NODE_NAME,
        SoundChanges::NODE_COLLECTION_NAME   => SoundChange::NODE_NAME,
        Cognates::NODE_COLLECTION_NAME       => Cognate::NODE_NAME,
    ];

    function getChildrenForNodeName(string $nodeName): array {
        return static::CHILDREN[$nodeName];
    }

    function getElementForCollectionName(string $collectionName): string {
        return static::ELEMENTS[$collectionName];
    }

    function getArrayKeyForNode(Node $node): string {
        return $this->getArrayKeyForNodeName($node::NODE_NAME);
    }

    function getArrayKeyForCollection(Nodes $node): string {
        return $this->getArrayKeyForNodeName($node::NODE_COLLECTION_NAME);
    }

    function getArrayKeyForNodeName(string $nodeName): string {
        if ($nodeName === HomographIndex::NODE_NAME) {
            return self::HOMOGRAPH_LABEL;
        }

        if ($nodeName === FormNodes::NODE_COLLECTION_NAME) {
            return self::FORMS_LABEL;
        }

        return $this->snakize($nodeName);
    }

    private function snakize(string $string): string {
        return str_replace(' ', '_', $string);
    }
}
