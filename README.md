# Dictionary: Array Parser

[![pipeline status](https://gitlab.com/Zalmoksis/dictionary-array-parser/badges/master/pipeline.svg)](https://gitlab.com/Zalmoksis/dictionary-array-parser/-/commits/master)
[![coverage report](https://gitlab.com/Zalmoksis/dictionary-array-parser/badges/master/coverage.svg)](https://gitlab.com/Zalmoksis/dictionary-array-parser/-/commits/master)

This repository contains interfaces for transforming dictionary model 
(described by [zalmoksis\dictionary-model](https://gitlab.com/Zalmoksis/dictionary))
to an array and the default implementation of it.

Interfaces:
- `ArraySerializer`
- `ArrayDeserializer`
- `ArrayNormalizer`

Default implementation:
- `DefaultArraySerializer`
- `DefaultArrayDeserializer`
- `DefaultArrayNormalizer`
