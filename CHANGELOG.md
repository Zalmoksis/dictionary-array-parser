# Changelog

All notable changes to this project will be documented in this file
in the format of [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

The project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [unreleased]
### Added
- Description and licence in composer.json
- PHP 8.1 in CI
- Badges on the read-me page
### Changed
- CI optimization
- PHPUnit version compatibility
### Fixed
- Changelog formatting

## [0.8.2] — 2020-03-06
### Fixed
- Minor coding style issues
### Refactored
- Unit tests

## [0.8.1] — 2020-03-06
### Fixed
- Error when deserializing collection shorthands

## [0.8.0] — 2020-03-02
### Changed
- `DefaultArraySerializer`, `DefaultArrayDeserializer` and `DefaultArrayNormalizer`
  require now an instance of `ArrayStructure` as parameter. 

## [0.7.0] — 2020-01-16
### Added
- Support for `SoundChanges` and `SoundChange`
- Strict types
### Fixed
- Missing unit tests

## [0.6.1] — 2019-12-18
### Fixed
- Code cleanup

## [0.6.0] — 2019-12-18
### Added
- Support for labels (`Variety`, `Register`, `Domain`) and improved `HomographIndex`

## [0.5.9] — 2019-12-16
### Changed
- Refactoring of `DefaultArrayDeserializer`

## [0.5.8] — 2019-12-15
### Changed
- Further refactoring of `DefaultArraySerializer`

## [0.5.7] — 2019-12-15
### Changed
- Refactoring of `DefaultArraySerializer`

## [0.5.6] — 2019-12-14
### Added
- Unit tests for shorthands

## [0.5.5] — 2019-12-13
### Added
- Even more minimal unit tests (`Collocation`)

## [0.5.4] — 2019-12-13
### Added
- Even more minimal unit tests (`Entry`, `Sense`)

## [0.5.3] — 2019-12-12
### Added
- More unit tests with minimal entry

## [0.5.2] — 2019-12-12
### Fixed
- `Derivatives` rendered only when `Antonym` present

## [0.5.1] — 2019-12-11
### Fixed
- Shorthands failing with error

## [0.5.0] — 2019-12-10
### Added
- Support for references: `Antonym`, `Synonym`, `Derivative`
- Less noise in Gitlab CI
- Testing lowest dependencies in Gitlab CI
- `DefaultArraySerializerConfiguration::getAllowedShorthands` returns `'*'`
  if `allowAllShorthands()` was called earlier.

## [0.4.0] — 2019-12-01
### Added
- Homograph index is validated by `DefaultArrayDeserializer` and `DefaultArrayNormalizer` and in result `HomographIsNotNonnegativeInteger` may be thrown.
### Changed
- Upgrade to PHP 7.4
- All classes are now `final`
- Strict typing of properties
- Improved PHP Unit configuration
- Less calls to `add()` on collections.
### Fixed
- Link in README

## [0.3.0] — 2019-02-25
### Added
- Support for `Context` in `Sense`

## [0.2.2] — 2019-02-24
### Fixed
- Forms shorthand tag in configuration is now correctly in plural

## [0.2.1] — 2019-02-24
### Fixed
- Shorthands for forms

## [0.2.0] — 2019-02-21
### Added
- Etymology representation with "etymons" and "cognates",
  but without support for hypotheticality marking.
- Shorthands for forms

## [0.1.0] — 2019-02-20
### Added
- Moving from [zalmoksis/dictionary-yaml-parser](https://gitlab.com/Zalmoksis/dictionary-yaml-parser)
