# To do

## Fixes (and refactoring)
- Splitting `DefaultSerializer` and `DefaultSubserializer` into subserializers
- Documentation of normalization process and aliases

## Minor
- Type error handling (normalization and serialization) 
- Moving shorthands to normalization
