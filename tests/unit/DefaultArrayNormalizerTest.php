<?php

/** @noinspection PhpDocSignatureInspection */

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Parser\ArrayParser\Tests\Unit;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Parser\ArrayParser\{ArrayNormalizer, DefaultArrayNormalizer, DefaultArrayStructure};

final class DefaultArrayNormalizerTest extends TestCase {
    protected ArrayNormalizer $normalizer;

    function setUp(): void {
        $this->normalizer = new DefaultArrayNormalizer(new DefaultArrayStructure());
    }

    function provideEntries(): array {
        return [
            'minimal entry' => [
                'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntry.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntry.php',
            ],
            'minimal entry with headword' => [
                'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithHeadword.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithHeadword.php',
            ],
            'minimal entry with pronunciation' => [
                'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithPronunciation.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithPronunciation.php',
            ],
            'minimal entry with category' => [
                'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithCategory.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithCategory.php',
            ],
            'minimal entry with forms' => [
                'providedEntry' => require __DIR__ . '/../samples/array/entry/entryWithForms.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/entryWithForms.php',
            ],
            'minimal entry with variety' => [
                'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithVariety.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithVariety.php',
            ],
            'minimal entry with register' => [
                'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithRegister.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithRegister.php',
            ],
            'minimal entry with domain' => [
                'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithDomain.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithDomain.php',
            ],
            'minimal entry with definition' => [
                'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithDefinition.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithDefinition.php',
            ],
            'minimal entry with translation' => [
                'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithTranslation.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithTranslation.php',
            ],
            'minimal entry with synonym' => [
                'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithSynonym.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithSynonym.php',
            ],
            'minimal entry with antonym' => [
                'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithAntonym.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithAntonym.php',
            ],
            'minimal entry with derivative' => [
                'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithDerivative.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithDerivative.php',
            ],
            'minimal entry with etymon language' => [
                'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithEtymonLanguage.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithEtymonLanguage.php',
            ],
            'minimal entry with etymon lemma' => [
                'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithEtymonLemma.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithEtymonLemma.php',
            ],
            'minimal entry with etymon gloss' => [
                'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithEtymonGloss.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithEtymonGloss.php',
            ],
            'minimal entry with sound change' => [
                'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithSoundChange.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithSoundChange.php',
            ],
            'minimal entry with cognate language' => [
                'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithCognateLanguage.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithCognateLanguage.php',
            ],
            'minimal entry with cognate lemma' => [
                'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithCognateLemma.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithCognateLemma.php',
            ],
            'minimal entry with cognate gloss' => [
                'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithCognateGloss.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithCognateGloss.php',
            ],
            'minimal entry with sense' => [
                'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithSense.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithSense.php',
            ],
            'full entry' => [
                'providedEntry' => require __DIR__ . '/../samples/array/entry/entry.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/entry.php',
            ],
            'entry with aliases' => [
                'providedEntry' => require __DIR__ . '/../samples/array/entry/entryWithAliases.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/entryWithAliasesResolved.php',
            ],
        ];
    }

    /**
     * @dataProvider provideEntries
     */
    function testNormalizeEntry($providedEntry, $expectedEntry): void {
        $this->assertEquals($expectedEntry, $this->normalizer->normalizeEntry($providedEntry));
    }

    function provideSenses(): array {
        return [
            'minimal sense' => [
                'providedEntry' => require __DIR__ . '/../samples/array/sense/minimalSense.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/sense/minimalSense.php',
            ],
            'minimal sense with variety' => [
                'providedEntry' => require __DIR__ . '/../samples/array/sense/minimalSenseWithVariety.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/sense/minimalSenseWithVariety.php',
            ],
            'minimal sense with register' => [
                'providedEntry' => require __DIR__ . '/../samples/array/sense/minimalSenseWithRegister.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/sense/minimalSenseWithRegister.php',
            ],
            'minimal sense with domain' => [
                'providedEntry' => require __DIR__ . '/../samples/array/sense/minimalSenseWithDomain.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/sense/minimalSenseWithDomain.php',
            ],
            'minimal sense with context' => [
                'providedEntry' => require __DIR__ . '/../samples/array/sense/minimalSenseWithContext.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/sense/minimalSenseWithContext.php',
            ],
            'minimal sense with definition' => [
                'providedEntry' => require __DIR__ . '/../samples/array/sense/minimalSenseWithDefinition.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/sense/minimalSenseWithDefinition.php',
            ],
            'minimal sense with translation' => [
                'providedEntry' => require __DIR__ . '/../samples/array/sense/minimalSenseWithTranslation.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/sense/minimalSenseWithTranslation.php',
            ],
            'minimal sense with synonym' => [
                'providedEntry' => require __DIR__ . '/../samples/array/sense/minimalSenseWithSynonym.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/sense/minimalSenseWithSynonym.php',
            ],
            'minimal sense with antonym' => [
                'providedEntry' => require __DIR__ . '/../samples/array/sense/minimalSenseWithAntonym.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/sense/minimalSenseWithAntonym.php',
            ],
            'minimal sense with sense' => [
                'providedEntry' => require __DIR__ . '/../samples/array/sense/minimalSenseWithSense.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/sense/minimalSenseWithSense.php',
            ],
            'full sense' => [
                'providedEntry' => require __DIR__ . '/../samples/array/sense/sense.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/sense/sense.php',
            ],
        ];
    }

    /**
     * @dataProvider provideSenses
     */
    function testNormalizeSense($providedSense, $expectedSense): void {
        $this->assertEquals($expectedSense, $this->normalizer->normalizeSense($providedSense));
    }

    function provideCollocations(): array {
        return [
            'minimal collocation' => [
                'providedEntry' => require __DIR__ . '/../samples/array/collocation/minimalCollocation.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/collocation/minimalCollocation.php',
            ],
            'minimal collocation with headword' => [
                'providedEntry' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithHeadword.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithHeadword.php',
            ],
            'minimal collocation with pronunciation' => [
                'providedEntry' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithDefinition.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithDefinition.php',
            ],
            'minimal collocation with variety' => [
                'providedEntry' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithVariety.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithVariety.php',
            ],
            'minimal collocation with register' => [
                'providedEntry' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithRegister.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithRegister.php',
            ],
            'minimal collocation with domain' => [
                'providedEntry' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithDomain.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithDomain.php',
            ],
            'minimal collocation with definition' => [
                'providedEntry' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithDefinition.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithDefinition.php',
            ],
            'minimal collocation with translation' => [
                'providedEntry' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithTranslation.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithTranslation.php',
            ],
            'minimal collocation with sense' => [
                'providedEntry' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithSense.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithSense.php',
            ],
            'full collocation' => [
                'providedEntry' => require __DIR__ . '/../samples/array/collocation/collocation.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/collocation/collocation.php',
            ],
        ];
    }

    /**
     * @dataProvider provideCollocations
     */
    function testNormalizeCollocation($providedCollocation, $expectedCollocation): void {
        $this->assertEquals($expectedCollocation, $this->normalizer->normalizeCollocation($providedCollocation));
    }
}
