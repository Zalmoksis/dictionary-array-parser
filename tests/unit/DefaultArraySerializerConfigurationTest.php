<?php

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Parser\ArrayParser\Tests\Unit;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Parser\ArrayParser\DefaultArraySerializerConfiguration;

final class DefaultArraySerializerConfigurationTest extends TestCase {

    public function testAllowingShorthands(): void {
        $configuration = (new DefaultArraySerializerConfiguration())->setAllowedShorthands('a', 'shorthand');
        self::assertTrue($configuration->isShorthandAllowed('a'));
        self::assertTrue($configuration->isShorthandAllowed('shorthand'));
        self::assertFalse($configuration->isShorthandAllowed('b'));
    }

    public function testAllowingAllShorthands(): void {
        $configuration = (new DefaultArraySerializerConfiguration())->allowAllShorthands();
        self::assertTrue($configuration->isShorthandAllowed('a'));
        self::assertTrue($configuration->isShorthandAllowed('shorthand'));
    }

    public function testGetAllowedShorthandsWithSpecifiedShorthands(): void {
        $configuration = (new DefaultArraySerializerConfiguration())->setAllowedShorthands('a', 'shorthand');
        self::assertSame(['a', 'shorthand'], $configuration->getAllowedShorthands());
    }

    public function testGetAllowedShorthandsWithAllShorthands(): void {
        $configuration = (new DefaultArraySerializerConfiguration())->allowAllShorthands();
        self::assertSame(['*'], $configuration->getAllowedShorthands());
    }
}
