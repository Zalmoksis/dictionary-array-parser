<?php

/** @noinspection PhpDocSignatureInspection */

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Parser\ArrayParser\Tests\Unit;

use Generator;
use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{Collocation, Entry, Sense};
use Zalmoksis\Dictionary\Parser\ArrayParser\{DefaultArrayDeserializer, DefaultArrayStructure};
use Zalmoksis\Dictionary\Parser\ArrayParser\Exceptions\HomographIsNotPositiveInteger;

final class DefaultArrayDeserializerTest extends TestCase {
    function provideEntries(): Generator {
        yield 'minimal entry' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntry.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntry.php',
        ];
        yield 'minimal entry with headword' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithHeadword.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithHeadword.php',
        ];
        yield 'minimal entry with pronunciation' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithPronunciation.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithPronunciation.php',
        ];
        yield 'minimal entry with category' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithCategory.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithCategory.php',
        ];
        yield 'minimal entry with forms' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithForms.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithForms.php',
        ];
        yield 'minimal collocation with variety' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithVariety.php',
            'expectedCollocation' => require __DIR__ . '/../samples/model/entry/minimalEntryWithVariety.php',
        ];
        yield 'minimal collocation with register' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithRegister.php',
            'expectedCollocation' => require __DIR__ . '/../samples/model/entry/minimalEntryWithRegister.php',
        ];
        yield 'minimal collocation with domain' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithDomain.php',
            'expectedCollocation' => require __DIR__ . '/../samples/model/entry/minimalEntryWithDomain.php',
        ];
        yield 'minimal entry with definition' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithDefinition.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithDefinition.php',
        ];
        yield 'minimal entry with translation' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithTranslation.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithTranslation.php',
        ];
        yield 'minimal entry with synonym' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithSynonym.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithSynonym.php',
        ];
        yield 'minimal entry with antonym' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithAntonym.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithAntonym.php',
        ];
        yield 'minimal entry with derivative' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithDerivative.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithDerivative.php',
        ];
        yield 'minimal entry with sense' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithSense.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithSense.php',
        ];
        yield 'minimal entry with etymon language' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithEtymonLanguage.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithEtymonLanguage.php',
        ];
        yield 'minimal entry with etymon lemma' => [
                'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
                'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithEtymonLemma.php',
                'expectedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithEtymonLemma.php',
        ];
        yield 'minimal entry with etymon gloss' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithEtymonGloss.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithEtymonGloss.php',
        ];
        yield 'minimal entry with sound change' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithSoundChange.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithSoundChange.php',
        ];
        yield 'minimal entry with cognate language' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithCognateLanguage.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithCognateLanguage.php',
        ];
        yield 'minimal entry with cognate lemma' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithCognateLemma.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithCognateLemma.php',
        ];
        yield 'minimal entry with cognate gloss' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithCognateGloss.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithCognateGloss.php',
        ];
        yield 'full entry' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/entry.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/entry.php',
        ];
        yield 'shorthands for headwords' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForHeadwords.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
        ];
        yield 'shorthands for pronunciations' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForPronunciations.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
        ];
        yield 'shorthands for categories' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForCategories.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
        ];
        yield 'shorthands for forms' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForForms.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
        ];
        yield 'shorthands for varieties' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForVarieties.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
        ];
        yield 'shorthands for registers' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForRegisters.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
        ];
        yield 'shorthands for domains' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForDomains.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
        ];
        yield 'shorthands for translations' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForTranslations.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
        ];
        yield 'shorthands for synonyms' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForSynonyms.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
        ];
        yield 'shorthands for antonyms' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForAntonyms.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
        ];
        yield 'shorthands for derivatives' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForDerivatives.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
        ];
        yield 'shorthands for sound changes' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForSoundChanges.php',
            'expectedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
        ];
    }

    /**
     * @dataProvider provideEntries
     */
    function testDeserializingEntry(
        DefaultArrayDeserializer $arrayDeserializer,
        array $providedEntry,
        Entry $expectedEntry
    ): void {
        static::assertEquals($expectedEntry, $arrayDeserializer->deserializeEntry($providedEntry));
    }

    function provideIncorrectEntries(): array {
        return [
            'negative homograph' => [
                'entry' => [
                    'headwords' => ['headword 1'],
                    'homograph' => -1,
                ],
                'exception' => HomographIsNotPositiveInteger::class,
            ],
            'zero homograph' => [
                'entry' => [
                    'headwords' => ['headword 1'],
                    'homograph' => 0,
                ],
                'exception' => HomographIsNotPositiveInteger::class,
            ],
            'non-integer string homograph' => [
                'entry' => [
                    'headwords' => ['headword 1'],
                    'homograph' => ['123kg']
                ],
                'exception' => HomographIsNotPositiveInteger::class,
            ],
        ];
    }

    /**
     * @dataProvider provideIncorrectEntries
     */
    function testDeserializingIncorrectEntry(array $entry, string $exception): void {
        $this->expectException($exception);
        (new DefaultArrayDeserializer(new DefaultArrayStructure()))->deserializeEntry($entry);
    }

    function provideSenses(): Generator {
        yield 'minimal collocation with variety' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedSense' => require __DIR__ . '/../samples/array/sense/minimalSenseWithVariety.php',
            'expectedSense' => require __DIR__ . '/../samples/model/sense/minimalSenseWithVariety.php',
        ];
        yield 'minimal collocation with register' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedSense' => require __DIR__ . '/../samples/array/sense/minimalSenseWithRegister.php',
            'expectedSense' => require __DIR__ . '/../samples/model/sense/minimalSenseWithRegister.php',
        ];
        yield 'minimal collocation with domain' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedSense' => require __DIR__ . '/../samples/array/sense/minimalSenseWithDomain.php',
            'expectedSense' => require __DIR__ . '/../samples/model/sense/minimalSenseWithDomain.php',
        ];
        yield 'minimal sense with context' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedSense' => require __DIR__ . '/../samples/array/sense/minimalSenseWithContext.php',
            'expectedSense' => require __DIR__ . '/../samples/model/sense/minimalSenseWithContext.php',
        ];
        yield 'minimal sense with definition' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedSense' => require __DIR__ . '/../samples/array/sense/minimalSenseWithDefinition.php',
            'expectedSense' => require __DIR__ . '/../samples/model/sense/minimalSenseWithDefinition.php',
        ];
        yield 'minimal sense with translation' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedSense' => require __DIR__ . '/../samples/array/sense/minimalSenseWithTranslation.php',
            'expectedSense' => require __DIR__ . '/../samples/model/sense/minimalSenseWithTranslation.php',
        ];
        yield 'minimal sense with synonym' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedSense' => require __DIR__ . '/../samples/array/sense/minimalSenseWithSynonym.php',
            'expectedSense' => require __DIR__ . '/../samples/model/sense/minimalSenseWithSynonym.php',
        ];
        yield 'minimal sense with antonym' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedSense' => require __DIR__ . '/../samples/array/sense/minimalSenseWithAntonym.php',
            'expectedSense' => require __DIR__ . '/../samples/model/sense/minimalSenseWithAntonym.php',
        ];
        yield 'minimal sense with sense' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedSense' => require __DIR__ . '/../samples/array/sense/minimalSenseWithSense.php',
            'expectedSense' => require __DIR__ . '/../samples/model/sense/minimalSenseWithSense.php',
        ];
        yield 'full sense' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedSense' => require __DIR__ . '/../samples/array/sense/sense.php',
            'expectedSense' => require __DIR__ . '/../samples/model/sense/sense.php',
        ];
    }

    /**
     * @dataProvider provideSenses
     */
    function testDeserializingSense(
        DefaultArrayDeserializer $arrayDeserializer,
        array $providedSense,
        Sense $expectedSense
    ): void {
        static::assertEquals($expectedSense, $arrayDeserializer->deserializeSense($providedSense));
    }

    function provideCollocations(): Generator {
        yield 'minimal collocation' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/array/collocation/minimalCollocation.php',
            'expectedCollocation' => require __DIR__ . '/../samples/model/collocation/minimalCollocation.php',
        ];
        yield 'minimal collocation with headword' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithHeadword.php',
            'expectedCollocation' => require __DIR__ . '/../samples/model/collocation/minimalCollocationWithHeadword.php',
        ];
        yield 'minimal collocation with pronunciation' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithPronunciation.php',
            'expectedCollocation' => require __DIR__ . '/../samples/model/collocation/minimalCollocationWithPronunciation.php',
        ];
        yield 'minimal collocation with variety' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithVariety.php',
            'expectedCollocation' => require __DIR__ . '/../samples/model/collocation/minimalCollocationWithVariety.php',
        ];
        yield 'minimal collocation with register' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithRegister.php',
            'expectedCollocation' => require __DIR__ . '/../samples/model/collocation/minimalCollocationWithRegister.php',
        ];
        yield 'minimal collocation with domain' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithDomain.php',
            'expectedCollocation' => require __DIR__ . '/../samples/model/collocation/minimalCollocationWithDomain.php',
        ];
        yield 'minimal collocation with definition' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithDefinition.php',
            'expectedCollocation' => require __DIR__ . '/../samples/model/collocation/minimalCollocationWithDefinition.php',
        ];
        yield 'minimal collocation with translation' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithTranslation.php',
            'expectedCollocation' => require __DIR__ . '/../samples/model/collocation/minimalCollocationWithTranslation.php',
        ];
        yield 'minimal collocation with sense' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithSense.php',
            'expectedCollocation' => require __DIR__ . '/../samples/model/collocation/minimalCollocationWithSense.php',
        ];
        yield 'full collocation' => [
            'deserializer' => new DefaultArrayDeserializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/array/collocation/collocation.php',
            'expectedCollocation' => require __DIR__ . '/../samples/model/collocation/collocation.php',
        ];
    }

    /**
     * @dataProvider provideCollocations
     */
    function testDeserializingCollocation(
        DefaultArrayDeserializer $arrayDeserializer,
        array $providedCollocation,
        Collocation $expectedCollocation
    ): void {
        static::assertEquals($expectedCollocation, $arrayDeserializer->deserializeCollocation($providedCollocation));
    }
}
