<?php

/** @noinspection PhpDocSignatureInspection */

declare(strict_types=1);

namespace Zalmoksis\Dictionary\Parser\ArrayParser\Tests\Unit;

use Generator;
use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Model\{Collocation, Entry, Sense};
use Zalmoksis\Dictionary\Parser\ArrayParser\DefaultArraySerializer;
use Zalmoksis\Dictionary\Parser\ArrayParser\DefaultArraySerializerConfiguration;
use Zalmoksis\Dictionary\Parser\ArrayParser\DefaultArrayStructure;

final class DefaultArraySerializerTest extends TestCase {
    function provideEntries(): Generator {
        yield 'minimal entry' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntry.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntry.php',
        ];
        yield 'minimal entry with headword' => [
                'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
                'providedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithHeadword.php',
                'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithHeadword.php',
            ];
        yield 'minimal entry with pronunciation' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithPronunciation.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithPronunciation.php',
        ];
        yield 'minimal entry with category' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithCategory.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithCategory.php',
        ];
        yield 'minimal entry with forms' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithForms.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithForms.php',
        ];
        yield 'minimal collocation with variety' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/model/entry/minimalEntryWithVariety.php',
            'expectedCollocation' => require __DIR__ . '/../samples/array/entry/minimalEntryWithVariety.php',
        ];
        yield 'minimal collocation with register' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/model/entry/minimalEntryWithRegister.php',
            'expectedCollocation' => require __DIR__ . '/../samples/array/entry/minimalEntryWithRegister.php',
        ];
        yield 'minimal collocation with domain' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/model/entry/minimalEntryWithDomain.php',
            'expectedCollocation' => require __DIR__ . '/../samples/array/entry/minimalEntryWithDomain.php',
        ];
        yield 'minimal entry with definition' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithDefinition.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithDefinition.php',
        ];
        yield 'minimal entry with translation' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithTranslation.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithTranslation.php',
        ];
        yield 'minimal entry with synonym' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithSynonym.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithSynonym.php',
        ];
        yield 'minimal entry with antonym' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithAntonym.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithAntonym.php',
        ];
        yield 'minimal entry with sense' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithSense.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithSense.php',
        ];
        yield 'minimal entry with derivative' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithDerivative.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithDerivative.php',
        ];
        yield 'minimal entry with etymon language' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithEtymonLanguage.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithEtymonLanguage.php',
        ];
        yield 'minimal entry with etymon lemma' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithEtymonLemma.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithEtymonLemma.php',
        ];
        yield 'minimal entry with etymon gloss' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithEtymonGloss.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithEtymonGloss.php',
        ];
        yield 'minimal entry with sound changes' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithSoundChange.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithSoundChange.php',
        ];
        yield 'minimal entry with cognate language' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithCognateLanguage.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithCognateLanguage.php',
        ];
        yield 'minimal entry with cognate lemma' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithCognateLemma.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithCognateLemma.php',
        ];
        yield 'minimal entry with cognate gloss' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/minimalEntryWithCognateGloss.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/minimalEntryWithCognateGloss.php',
        ];
        yield 'full entry' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/entry.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/entry.php',
        ];
        yield 'entry with default shorthands (no shorthands)' => [
            'deserializer' => new DefaultArraySerializer(
                new DefaultArrayStructure(),
                (new DefaultArraySerializerConfiguration())
            ),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/entryWithNoShorthands.php',
        ];
        yield 'entry with no shorthands' => [
            'deserializer' => new DefaultArraySerializer(
                new DefaultArrayStructure(),
                (new DefaultArraySerializerConfiguration())
                    ->setAllowedShorthands()
            ),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/entryWithNoShorthands.php',
        ];
        yield 'entry with shorthands for headwords' => [
            'deserializer' => new DefaultArraySerializer(
                new DefaultArrayStructure(),
                (new DefaultArraySerializerConfiguration())
                    ->setAllowedShorthands('headwords')
            ),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForHeadwords.php',
        ];
        yield 'entry with shorthands for pronunciation' => [
            'deserializer' => new DefaultArraySerializer(
                new DefaultArrayStructure(),
                (new DefaultArraySerializerConfiguration())
                    ->setAllowedShorthands('pronunciations')
            ),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForPronunciations.php',
        ];
        yield 'entry with shorthands for categories' => [
            'deserializer' => new DefaultArraySerializer(
                new DefaultArrayStructure(),
                (new DefaultArraySerializerConfiguration())
                    ->setAllowedShorthands('categories')
            ),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForCategories.php',
        ];
        yield 'entry with shorthands for forms' => [
            'deserializer' => new DefaultArraySerializer(
                new DefaultArrayStructure(),
                (new DefaultArraySerializerConfiguration())
                    ->setAllowedShorthands('forms')
            ),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForForms.php',
        ];
        yield 'entry with shorthands for varieties' => [
            'deserializer' => new DefaultArraySerializer(
                new DefaultArrayStructure(),
                (new DefaultArraySerializerConfiguration())
                    ->setAllowedShorthands('varieties')
            ),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForVarieties.php',
        ];
        yield 'entry with shorthands for registers' => [
            'deserializer' => new DefaultArraySerializer(
                new DefaultArrayStructure(),
                (new DefaultArraySerializerConfiguration())
                    ->setAllowedShorthands('registers')
            ),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForRegisters.php',
        ];
        yield 'entry with shorthands for domains' => [
            'deserializer' => new DefaultArraySerializer(
                new DefaultArrayStructure(),
                (new DefaultArraySerializerConfiguration())
                    ->setAllowedShorthands('domains')
            ),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForDomains.php',
        ];
        yield 'entry with shorthands for translations' => [
            'deserializer' => new DefaultArraySerializer(
                new DefaultArrayStructure(),
                (new DefaultArraySerializerConfiguration())
                    ->setAllowedShorthands('translations')
            ),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForTranslations.php',
        ];
        yield 'entry with shorthands for synonyms' => [
            'deserializer' => new DefaultArraySerializer(
                new DefaultArrayStructure(),
                (new DefaultArraySerializerConfiguration())
                    ->setAllowedShorthands('synonyms')
            ),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForSynonyms.php',
        ];
        yield 'entry with shorthands for antonyms' => [
            'deserializer' => new DefaultArraySerializer(
                new DefaultArrayStructure(),
                (new DefaultArraySerializerConfiguration())
                    ->setAllowedShorthands('antonyms')
            ),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForAntonyms.php',
        ];
        yield 'entry with shorthands for derivatives' => [
            'deserializer' => new DefaultArraySerializer(
                new DefaultArrayStructure(),
                (new DefaultArraySerializerConfiguration())
                    ->setAllowedShorthands('derivatives')
            ),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForDerivatives.php',
        ];
        yield 'entry with shorthands for sound changes' => [
            'deserializer' => new DefaultArraySerializer(
                new DefaultArrayStructure(),
                (new DefaultArraySerializerConfiguration())
                    ->setAllowedShorthands('sound_changes')
            ),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/entryWithShorthandsForSoundChanges.php',
        ];
        yield 'entry with all shorthands (explicit)' => [
            'deserializer' => new DefaultArraySerializer(
                new DefaultArrayStructure(),
                (new DefaultArraySerializerConfiguration())
                    ->setAllowedShorthands('*')
            ),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/entryWithAllShorthands.php',
        ];
        yield 'entry with all shorthands (allowAllShorthands)' => [
            'deserializer' => new DefaultArraySerializer(
                new DefaultArrayStructure(),
                (new DefaultArraySerializerConfiguration())
                    ->allowAllShorthands()
            ),
            'providedEntry' => require __DIR__ . '/../samples/model/entry/entryWithPossibleShorthands.php',
            'expectedEntry' => require __DIR__ . '/../samples/array/entry/entryWithAllShorthands.php',
        ];
    }

    /**
     * @dataProvider provideEntries
     */
    function testSerializingEntry(
        DefaultArraySerializer $arrayDeserializer,
        Entry $providedEntry,
        array $expectedEntry
    ): void {
        static::assertEquals($expectedEntry, $arrayDeserializer->serializeEntry($providedEntry));
    }

    function provideSenses(): Generator {
        yield 'minimal collocation with variety' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/model/sense/minimalSenseWithVariety.php',
            'expectedCollocation' => require __DIR__ . '/../samples/array/sense/minimalSenseWithVariety.php',
        ];
        yield 'minimal collocation with register' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/model/sense/minimalSenseWithRegister.php',
            'expectedCollocation' => require __DIR__ . '/../samples/array/sense/minimalSenseWithRegister.php',
        ];
        yield 'minimal collocation with domain' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/model/sense/minimalSenseWithDomain.php',
            'expectedCollocation' => require __DIR__ . '/../samples/array/sense/minimalSenseWithDomain.php',
        ];
        yield 'minimal sense with context' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedSense' => require __DIR__ . '/../samples/model/sense/minimalSenseWithContext.php',
            'expectedSense' => require __DIR__ . '/../samples/array/sense/minimalSenseWithContext.php',
        ];
        yield 'minimal sense with definition' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedSense' => require __DIR__ . '/../samples/model/sense/minimalSenseWithDefinition.php',
            'expectedSense' => require __DIR__ . '/../samples/array/sense/minimalSenseWithDefinition.php',
        ];
        yield 'minimal sense with translation' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedSense' => require __DIR__ . '/../samples/model/sense/minimalSenseWithTranslation.php',
            'expectedSense' => require __DIR__ . '/../samples/array/sense/minimalSenseWithTranslation.php',
        ];
        yield 'minimal sense with synonym' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedSense' => require __DIR__ . '/../samples/model/sense/minimalSenseWithSynonym.php',
            'expectedSense' => require __DIR__ . '/../samples/array/sense/minimalSenseWithSynonym.php',
        ];
        yield 'minimal sense with antonym' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedSense' => require __DIR__ . '/../samples/model/sense/minimalSenseWithAntonym.php',
            'expectedSense' => require __DIR__ . '/../samples/array/sense/minimalSenseWithAntonym.php',
        ];
        yield 'full sense' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedSense' => require __DIR__ . '/../samples/model/sense/sense.php',
            'expectedSense' => require __DIR__ . '/../samples/array/sense/sense.php',
        ];
    }

    /**
     * @dataProvider provideSenses
     */
    function testSerializingSense(
        DefaultArraySerializer $arraySerializer,
        Sense $providedSense,
        array $expectedSense
    ): void {
        static::assertEquals($expectedSense, $arraySerializer->serializeSense($providedSense));
    }

    function provideCollocations(): Generator {
        yield 'minimal collocation' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/model/collocation/minimalCollocation.php',
            'expectedCollocation' => require __DIR__ . '/../samples/array/collocation/minimalCollocation.php',
        ];
        yield 'minimal collocation with headword' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/model/collocation/minimalCollocationWithHeadword.php',
            'expectedCollocation' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithHeadword.php',
        ];
        yield 'minimal collocation with pronunciation' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/model/collocation/minimalCollocationWithPronunciation.php',
            'expectedCollocation' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithPronunciation.php',
        ];
        yield 'minimal collocation with variety' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/model/collocation/minimalCollocationWithVariety.php',
            'expectedCollocation' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithVariety.php',
        ];
        yield 'minimal collocation with register' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/model/collocation/minimalCollocationWithRegister.php',
            'expectedCollocation' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithRegister.php',
        ];
        yield 'minimal collocation with domain' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/model/collocation/minimalCollocationWithDomain.php',
            'expectedCollocation' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithDomain.php',
        ];
        yield 'minimal collocation with definition' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/model/collocation/minimalCollocationWithDefinition.php',
            'expectedCollocation' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithDefinition.php',
        ];
        yield 'minimal collocation with translation' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/model/collocation/minimalCollocationWithTranslation.php',
            'expectedCollocation' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithTranslation.php',
        ];
        yield 'minimal collocation with sense' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/model/collocation/minimalCollocationWithSense.php',
            'expectedCollocation' => require __DIR__ . '/../samples/array/collocation/minimalCollocationWithSense.php',
        ];
        yield 'full collocation' => [
            'deserializer' => new DefaultArraySerializer(new DefaultArrayStructure()),
            'providedCollocation' => require __DIR__ . '/../samples/model/collocation/collocation.php',
            'expectedCollocation' => require __DIR__ . '/../samples/array/collocation/collocation.php',
        ];
    }

    /**
     * @dataProvider provideCollocations
     */
    function testSerializingCollocation(
        DefaultArraySerializer $arraySerializer,
        Collocation $providedCollocation,
        array $expectedCollocation
    ): void {
        static::assertEquals($expectedCollocation, $arraySerializer->serializeCollocation($providedCollocation));
    }
}
