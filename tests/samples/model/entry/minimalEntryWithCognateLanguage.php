<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Cognate, Collections\Cognates, Entry, Language};

return (new Entry())
    ->setCognates(new Cognates(
        (new Cognate())->setLanguage(new Language('language'))
    ))
;
