<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Category, Collections\Categories, Entry};

return (new Entry())
    ->setCategories(new Categories(new Category('category')))
;
