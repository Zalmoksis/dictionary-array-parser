<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Entry, Form, FormLabel, Headword, Translation};
use Zalmoksis\Dictionary\Model\Collections\{FormNodes, Headwords, Translations};

return (new Entry())
    ->setHeadwords(new Headwords(new Headword('headword 1')))
    ->setFormNodes(
        new FormNodes(
            new Form(
                new FormLabel('form label 1'),
                new Headwords(
                    new Headword('form 1')
                )
            )
        )
    )
    ->setTranslations(new Translations(new Translation('translation 1')))
;
