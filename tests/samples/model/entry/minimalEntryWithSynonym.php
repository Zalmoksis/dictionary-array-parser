<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Synonyms, Synonym, Entry};

return (new Entry())
    ->setSynonyms(new Synonyms(new Synonym('synonym')))
;
