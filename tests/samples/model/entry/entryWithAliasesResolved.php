<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{
    Category,
    Collocation,
    Definition,
    Entry,
    Form,
    FormLabel,
    Headword,
    HomographIndex,
    Pronunciation,
    Sense,
    Translation,
};
use Zalmoksis\Dictionary\Model\Collections\{
    Categories,
    Collocations,
    FormNodes,
    Headwords,
    Pronunciations,
    Senses,
    Translations,
};

return (new Entry())
    ->setHeadwords(new Headwords(
        new Headword('headword 1'),
        new Headword('ˈhɛdˌwɜːd 2'),
        new Headword('headword 3'),
        new Headword('ˈhɛdˌwɜːd 4'),
        new Headword('headword 5'),
        new Headword('ˈhɛdˌwɜːd 6'),
        new Headword('headword 7'),
        new Headword('ˈhɛdˌwɜːd 8'),
        new Headword('headword 9'),
        new Headword('ˈhɛdˌwɜːd 10'),
    ))
    ->setHomographIndex(new HomographIndex(3))
    ->setPronunciations(new Pronunciations(
        new Pronunciation('pronunciation 1'),
        new Pronunciation('prəˌnʌnsɪˈeɪʃən 2'),
        new Pronunciation('pronunciation 3'),
        new Pronunciation('prəˌnʌnsɪˈeɪʃən 4'),
        new Pronunciation('pronunciation 5'),
        new Pronunciation('prəˌnʌnsɪˈeɪʃən 6'),
        new Pronunciation('pronunciation 7'),
        new Pronunciation('prəˌnʌnsɪˈeɪʃən 8'),
    ))
    ->setCategories(new Categories(
        new Category('category 1'),
        new Category('ˈkætɪɡərɪ 2'),
        new Category('category 3'),
        new Category('ˈkætɪɡərɪ 4'),
        new Category('category 5'),
        new Category('ˈkætɪɡərɪ 6'),
        new Category('category 7'),
        new Category('ˈkætɪɡərɪ 8'),
    ))
    ->setFormNodes(new FormNodes(
        (new Form(new FormLabel('form label 1')))
            ->setHeadwords(new Headwords(
                new Headword('form 1.1'),
                new Headword('fɔːm 1.2'),
            )),
        (new Form(new FormLabel('form label 2')))
            ->setHeadwords(new Headwords(
                new Headword('form 2.1'),
                new Headword('fɔːm 2.2'),
            )),
        (new Form(new FormLabel('form label 3')))
            ->setHeadwords(new Headwords(
                new Headword('form 3.1'),
                new Headword('fɔːm 3.2'),
            ))
    ))
    ->setDefinition(new Definition('definition 0'))
    ->setTranslations(new Translations(
        new Translation('translation 1'),
        new Translation('trænsˈleɪʃən 2'),
        new Translation('translation 3'),
        new Translation('trænsˈleɪʃən 4'),
        new Translation('translation 5'),
        new Translation('trænsˈleɪʃən 6'),
        new Translation('translation 7'),
        new Translation('trænsˈleɪʃən 8'),
        new Translation('translation 9'),
        new Translation('trænsˈleɪʃən 10'),
        new Translation('translation 11'),
        new Translation('trænsˈleɪʃən 12'),
    ))
    ->setCollocations(new Collocations(
        (new Collocation())
            ->setHeadwords(new Headwords(new Headword('headword 0.a.1')))
            ->setTranslations(new Translations(new Translation('translation 0.a.1'))),
        (new Collocation())
            ->setHeadwords(new Headwords(new Headword('headword 0.b.1')))
            ->setTranslations(new Translations(new Translation('translation 0.b.1'))),
        (new Collocation())
            ->setHeadwords(new Headwords(new Headword('headword 0.c.1')))
            ->setTranslations(new Translations(new Translation('translation 0.c.1'))),
        (new Collocation())
            ->setHeadwords(new Headwords(new Headword('headword 0.d.1')))
            ->setTranslations(new Translations(new Translation('translation 0.d.1'))),
        (new Collocation())
            ->setHeadwords(new Headwords(new Headword('headword 0.e.1')))
            ->setTranslations(new Translations(new Translation('translation 0.e.1'))),
    ))
    ->setSenses(new Senses(
        (new Sense())
            ->setDefinition(new Definition('definition 1'))
            ->setTranslations(new Translations(new Translation('translation 1.1'))),
        (new Sense())
            ->setDefinition(new Definition('definition 2'))
            ->setTranslations(new Translations(new Translation('translation 2.1'))),
        (new Sense())
            ->setDefinition(new Definition('definition 3'))
            ->setTranslations(new Translations(new Translation('translation 3.1'))),
    ))
;
