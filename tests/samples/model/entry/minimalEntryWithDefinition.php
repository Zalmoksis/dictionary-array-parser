<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Definition, Entry};

return (new Entry())
    ->setDefinition(new Definition('definition'))
;
