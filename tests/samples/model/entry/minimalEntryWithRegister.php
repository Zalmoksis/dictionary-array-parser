<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Registers, Entry, Register};

return (new Entry())
    ->setRegisters(new Registers(new Register('register')))
;
