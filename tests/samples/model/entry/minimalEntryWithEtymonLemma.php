<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Etymons, Entry, Etymon, Lemma};

return (new Entry())
    ->setEtymons(new Etymons(
        (new Etymon())->setLemma(new Lemma('lemma'))
    ))
;
