<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Cognate, Collections\Cognates, Entry, Lemma};

return (new Entry())
    ->setCognates(new Cognates(
        (new Cognate())->setLemma(new Lemma('lemma'))
    ))
;
