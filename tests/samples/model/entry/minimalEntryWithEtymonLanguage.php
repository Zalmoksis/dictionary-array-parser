<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Etymons, Entry, Etymon, Language};

return (new Entry())
    ->setEtymons(new Etymons(
        (new Etymon())->setLanguage(new Language('language'))
    ))
;
