<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Etymons, Entry, Etymon, Gloss};

return (new Entry())
    ->setEtymons(new Etymons(
        (new Etymon())->setGloss(new Gloss('gloss'))
    ))
;
