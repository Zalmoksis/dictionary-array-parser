<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\SoundChanges, Entry, SoundChange};

return (new Entry())
    ->setSoundChanges(new SoundChanges(new SoundChange('sound change')))
;
