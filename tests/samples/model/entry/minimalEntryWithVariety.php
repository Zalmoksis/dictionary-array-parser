<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Varieties, Entry, Variety};

return (new Entry())
    ->setVarieties(new Varieties(new Variety('variety')))
;
