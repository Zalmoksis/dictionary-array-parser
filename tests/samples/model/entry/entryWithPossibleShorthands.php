<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{
    Antonym,
    Category,
    Derivative,
    Domain,
    Entry,
    Form,
    FormGroup,
    FormLabel,
    Headword,
    Pronunciation,
    Register,
    Sense,
    SoundChange,
    Synonym,
    Translation,
    Variety,
};
use Zalmoksis\Dictionary\Model\Collections\{
    Antonyms,
    Categories,
    Derivatives,
    Domains,
    FormNodes,
    Headwords,
    Pronunciations,
    Registers,
    Senses,
    SoundChanges,
    Synonyms,
    Translations,
    Varieties,
};

return (new Entry())
    ->setHeadwords(new Headwords(new Headword('ˈhɛdˌwɜːd')))
    ->setPronunciations(new Pronunciations(new Pronunciation('prəˌnʌnsɪˈeɪʃən')))
    ->setCategories(new Categories(new Category('ˈkætɪɡərɪ')))
    ->setFormNodes(new FormNodes(
        (new Form(new FormLabel('form label 1')))
            ->setHeadwords(new Headwords(new Headword('fɔːm 1'))),
        (new FormGroup(new FormLabel('form label 2')))
            ->setFormNodes(new FormNodes(
                (new Form(new FormLabel('fɔːm ˈleɪ bəl 2.1')))
                    ->setHeadwords(new Headwords(new Headword('fɔːm 2.1')))
            ))
    ))
    ->setVarieties(new Varieties(new Variety('variety')))
    ->setRegisters(new Registers(new Register('register')))
    ->setDomains(new Domains(new Domain('domain')))
    ->setTranslations(new Translations(new Translation('trænsˈleɪʃən')))
    ->setSynonyms(new Synonyms(new Synonym('ˈsɪnəˌnɪm')))
    ->setAntonyms(new Antonyms(new Antonym('ˈæntənɪm')))
    /* TODO: some collocations should go here */
    ->setSenses(new Senses(
        (new Sense())
            ->setTranslations(new Translations(new Translation('trænsˈleɪʃən 1'))),
        (new Sense())
            ->setTranslations(new Translations(new Translation('trænsˈleɪʃən 2'))),
    ))
    ->setDerivatives(new Derivatives(new Derivative('derivative')))
    ->setSoundChanges(new SoundChanges(new SoundChange('saʊnd tʃeɪndʒ')))
;
