<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Domains, Entry, Domain};

return (new Entry())
    ->setDomains(new Domains(new Domain('domain')))
;
