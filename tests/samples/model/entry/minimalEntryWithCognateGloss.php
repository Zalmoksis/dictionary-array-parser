<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Cognate, Collections\Cognates, Entry, Gloss};

return (new Entry())
    ->setCognates(new Cognates(
        (new Cognate())->setGloss(new Gloss('gloss'))
    ))
;
