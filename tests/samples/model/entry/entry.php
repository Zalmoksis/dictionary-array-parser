<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{
    Antonym,
    Category,
    Cognate,
    Collocation,
    Context,
    Definition,
    Derivative,
    Domain,
    Entry,
    Etymon,
    Form,
    FormGroup,
    FormLabel,
    Gloss,
    Headword,
    HomographIndex,
    Language,
    Lemma,
    Pronunciation,
    Register,
    Sense,
    SoundChange,
    Synonym,
    Translation,
    Variety,
};
use Zalmoksis\Dictionary\Model\Collections\{
    Antonyms,
    Categories,
    Cognates,
    Collocations,
    Derivatives,
    Domains,
    Etymons,
    FormNodes,
    Headwords,
    Pronunciations,
    Registers,
    Senses,
    SoundChanges,
    Synonyms,
    Translations,
    Varieties,
};

return (new Entry())
    ->setHeadwords(new Headwords(
        new Headword('headword 1'),
        new Headword('ˈhɛdˌwɜːd 2'),
    ))
    ->setHomographIndex(new HomographIndex(3))
    ->setPronunciations(new Pronunciations(
        new Pronunciation('pronunciation 1'),
        new Pronunciation('prəˌnʌnsɪˈeɪʃən 2'),
    ))
    ->setCategories(new Categories(new Category('category 1'), new Category('ˈkætɪɡərɪ 2')))
    ->setFormNodes(new FormNodes(
        (new Form(new FormLabel('form label 1')))
            ->setHeadwords(new Headwords(
                new Headword('form 1.1'),
                new Headword('fɔːm 1.2'),
            )),
        (new FormGroup(new FormLabel('form label 2')))
            ->setFormNodes(
                new FormNodes(
                    (new Form(new FormLabel('fɔːm ˈleɪ bəl 2.1')))
                        ->setHeadwords(new Headwords(
                            new Headword('form 2.1.1'),
                            new Headword('fɔːm 2.1.2'),
                        ))
                )
            )
    ))
    ->setVarieties(new Varieties(
        new Variety('variety 1'),
        new Variety('variety 2'),
    ))
    ->setRegisters(new Registers(
        new Register('register 1'),
        new Register('register 2'),
    ))
    ->setDomains(new Domains(
        new Domain('domain 1'),
        new Domain('domain 2'),
    ))
    ->setDefinition(new Definition('definition'))
    ->setTranslations(new Translations(
        new Translation('translation 1'),
        new Translation('trænsˈleɪʃən 2'),
    ))
    ->setSynonyms(new Synonyms(
        new Synonym('synonym 1'),
        new Synonym('synonym 2'),
    ))
    ->setAntonyms(new Antonyms(
        new Antonym('antonym 1'),
        new Antonym('antonym 2'),
    ))
    ->setCollocations(new Collocations(
        (new Collocation())
            ->setHeadwords(new Headwords(
                new Headword('headword a.1'),
                new Headword('ˈhɛdˌwɜːd a.2'),
            ))
            ->setPronunciations(new Pronunciations(
                new Pronunciation('pronunciation a.1'),
                new Pronunciation('prəˌnʌnsɪˈeɪʃən a.2'),
            ))
            ->setDefinition(new Definition('definition a'))
            ->setTranslations(new Translations(
                new Translation('translation a.1'),
                new Translation('trænsˈleɪʃən a.2'),
            ))
            ->setSenses(new Senses(
                (new Sense())
                    ->setDefinition(new Definition('definition a.1'))
                    ->setTranslations(new Translations(
                        new Translation('translation a.1.1'),
                        new Translation('trænsˈleɪʃən a.1.2'),
                    )),
                (new Sense())
                    ->setDefinition(new Definition('ˌdɛfɪˈnɪʃən a.2'))
                    ->setTranslations(new Translations(
                        new Translation('translation a.2.1'),
                        new Translation('trænsˈleɪʃən a.2.2'),
                    )),
            )),
        (new Collocation())
            ->setHeadwords(new Headwords(
                new Headword('headword b.1'),
                new Headword('ˈhɛdˌwɜːd b.2'),
            ))
            ->setPronunciations(new Pronunciations(
                new Pronunciation('pronunciation b.1'),
                new Pronunciation('prəˌnʌnsɪˈeɪʃən b.2'),
            ))
            ->setDefinition(new Definition('ˌdɛfɪˈnɪʃən b'))
            ->setTranslations(new Translations(
                new Translation('translation b.1'),
                new Translation('trænsˈleɪʃən b.2'),
            ))
    ))
    ->setSenses(new Senses(
        (new Sense())
            ->setContext(new Context('ˈkɒntekst 1'))
            ->setDefinition(new Definition('definition 1'))
            ->setTranslations(new Translations(
                new Translation('translation 1.1'),
                new Translation('trænsˈleɪʃən 1.2'),
            ))
            ->setSynonyms(new Synonyms(
                new Synonym('synonym 1.1'),
                new Synonym('synonym 1.2'),
            ))
            ->setAntonyms(new Antonyms(
                new Antonym('antonym 1.1'),
                new Antonym('antonym 1.2'),
            ))
            ->setCollocations(new Collocations(
                (new Collocation())
                    ->setHeadwords(new Headwords(
                        new Headword('headword 1.a.1'),
                        new Headword('ˈhɛdˌwɜːd 1.a.2'),
                    ))
                    ->setPronunciations(new Pronunciations(
                        new Pronunciation('pronunciation 1.a.1'),
                        new Pronunciation('prəˌnʌnsɪˈeɪʃən 1.a.2'),
                    ))
                    ->setDefinition(new Definition('definition 1.a'))
                    ->setTranslations(new Translations(
                        new Translation('translation 1.a.1'),
                        new Translation('trænsˈleɪʃən 1.a.2'),
                    ))
                    ->setSenses(new Senses(
                        (new Sense())
                            ->setDefinition(new Definition('definition 1.a.1'))
                            ->setTranslations(new Translations(
                                new Translation('translation 1.a.1.1'),
                                new Translation('trænsˈleɪʃən 1.a.1.2'),
                            )),
                        (new Sense())
                            ->setDefinition(new Definition('ˌdɛfɪˈnɪʃən 1.a.2'))
                            ->setTranslations(new Translations(
                                new Translation('translation 1.a.2.1'),
                                new Translation('trænsˈleɪʃən 1.a.2.2'),
                            )),
                    )),
                (new Collocation())
                    ->setHeadwords(new Headwords(
                        new Headword('headword 1.b.1'),
                        new Headword('ˈhɛdˌwɜːd 1.b.2'),
                    ))
                    ->setPronunciations(new Pronunciations(
                        new Pronunciation('pronunciation 1.b.1'),
                        new Pronunciation('prəˌnʌnsɪˈeɪʃən 1.b.2'),
                    ))
                    ->setDefinition(new Definition('ˌdɛfɪˈnɪʃən 1.b'))
                    ->setTranslations(new Translations(
                        new Translation('translation 1.b.1'),
                        new Translation('trænsˈleɪʃən 1.b.2'),
                    ))
            )),
        (new Sense())
            ->setDefinition(new Definition('ˌdɛfɪˈnɪʃən 2'))
            ->setTranslations(new Translations(
                new Translation('translation 2.1'),
                new Translation('trænsˈleɪʃən 2.2'),
            ))
    ))
    ->setDerivatives(new Derivatives(
        new Derivative('derivative 1'),
        new Derivative('derivative 2'),
    ))
    ->setEtymons(new Etymons(
        (new Etymon())
            ->setLanguage(new Language('language 1'))
            ->setLemma(new Lemma('lemma 1'))
            ->setGloss(new Gloss('gloss 1')),
        (new Etymon())
            ->setLemma(new Lemma('lemma 2')),
    ))
    ->setSoundChanges(new SoundChanges(
        new SoundChange('sound change 1'),
        new SoundChange('saʊnd tʃeɪndʒ 2'),
    ))
    ->setCognates(new Cognates(
        (new Cognate())
            ->setLanguage(new Language('language 1'))
            ->setLemma(new Lemma('lemma 1'))
            ->setGloss(new Gloss('gloss 1')),
        (new Cognate())
            ->setLanguage(new Language('language 2'))
            ->setLemma(new Lemma('lemma 2')),
    ))
;
