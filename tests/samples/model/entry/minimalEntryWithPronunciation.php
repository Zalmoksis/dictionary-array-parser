<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Pronunciations, Entry, Pronunciation};

return (new Entry())
    ->setPronunciations(new Pronunciations(new Pronunciation('pronunciation')))
;
