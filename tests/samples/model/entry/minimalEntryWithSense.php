<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Senses, Entry, Sense};

return (new Entry())
    ->setSenses(new Senses(new Sense()))
;
