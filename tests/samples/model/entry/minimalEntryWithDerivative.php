<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Derivatives, Derivative, Entry};

return (new Entry())
    ->setDerivatives(new Derivatives(new Derivative('derivative')))
;
