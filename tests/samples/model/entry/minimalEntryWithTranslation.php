<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Translations, Entry, Translation};

return (new Entry())
    ->setTranslations(new Translations(new Translation('translation')))
;
