<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Antonyms, Antonym, Entry};

return (new Entry())
    ->setAntonyms(new Antonyms(new Antonym('antonym')))
;
