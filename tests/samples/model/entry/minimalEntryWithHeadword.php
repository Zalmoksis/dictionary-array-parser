<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Headwords, Entry, Headword};

return (new Entry())
    ->setHeadwords(new Headwords(new Headword('headword')))
;
