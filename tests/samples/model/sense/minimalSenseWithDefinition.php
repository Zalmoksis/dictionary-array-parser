<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Definition, Sense};

return (new Sense())
    ->setDefinition(new Definition('definition'))
;
