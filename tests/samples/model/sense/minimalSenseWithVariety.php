<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Varieties, Sense, Variety};

return (new Sense())
    ->setVarieties(new Varieties(new Variety('variety')))
;
