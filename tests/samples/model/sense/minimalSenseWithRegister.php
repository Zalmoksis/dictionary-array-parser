<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Registers, Sense, Register};

return (new Sense())
    ->setRegisters(new Registers(new Register('register')))
;
