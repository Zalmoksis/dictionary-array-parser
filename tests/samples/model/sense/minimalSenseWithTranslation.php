<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Translations, Sense, Translation};

return (new Sense())
    ->setTranslations(new Translations(new Translation('translation')))
;
