<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Synonyms, Sense, Synonym};

return (new Sense())
    ->setSynonyms(new Synonyms(new Synonym('synonym')))
;
