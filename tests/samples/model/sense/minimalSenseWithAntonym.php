<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Antonyms, Sense, Antonym};

return (new Sense())
    ->setAntonyms(new Antonyms(new Antonym('antonym')))
;
