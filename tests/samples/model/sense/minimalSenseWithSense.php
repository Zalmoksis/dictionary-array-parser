<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Senses, Sense};

return (new Sense())->setSenses(new Senses(new Sense()));
