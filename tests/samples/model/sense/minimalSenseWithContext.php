<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Context, Sense};

return (new Sense())
    ->setContext(new Context('context'))
;
