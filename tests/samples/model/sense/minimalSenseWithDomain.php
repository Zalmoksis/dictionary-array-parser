<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Domains, Sense, Domain};

return (new Sense())
    ->setDomains(new Domains(new Domain('domain')))
;
