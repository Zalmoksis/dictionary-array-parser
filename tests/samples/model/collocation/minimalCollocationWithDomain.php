<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Domains, Collocation, Domain};

return (new Collocation())
    ->setDomains(new Domains(new Domain('domain')))
;
