<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Translations, Collocation, Translation};

return (new Collocation())
    ->setTranslations(new Translations(new Translation('translation')))
;
