<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Senses, Collocation, Sense};

return (new Collocation())
    ->setSenses(new Senses(new Sense()))
;
