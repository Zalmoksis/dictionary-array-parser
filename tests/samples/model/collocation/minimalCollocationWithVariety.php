<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Varieties, Collocation, Variety};

return (new Collocation())
    ->setVarieties(new Varieties(new Variety('variety')))
;
