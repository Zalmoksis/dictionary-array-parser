<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Pronunciations, Collocation, Pronunciation};

return (new Collocation())
    ->setPronunciations(new Pronunciations(new Pronunciation('pronunciation')))
;
