<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Headwords, Collocation, Headword};

return (new Collocation())
    ->setHeadwords(new Headwords(new Headword('headword')))
;
