<?php

declare(strict_types=1);

use Zalmoksis\Dictionary\Model\{Collections\Registers, Collocation, Register};

return (new Collocation())
    ->setRegisters(new Registers(new Register('register')))
;
