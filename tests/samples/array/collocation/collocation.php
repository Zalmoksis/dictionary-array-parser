<?php

declare(strict_types=1);

return [
    'headwords' => [
        'headword a.1',
        'ˈhɛdˌwɜːd a.2',
    ],
    'pronunciations' => [
        'pronunciation a.1',
        'prəˌnʌnsɪˈeɪʃən a.2',
    ],
    'varieties' => [
        'variety 1',
        'variety 2',
    ],
    'registers' => [
        'register 1',
        'register 2',
    ],
    'domains' => [
        'domain 1',
        'domain 2',
    ],
    'definition' => 'ˌdɛfɪˈnɪʃən a',
    'translations' => [
        'translation a.1',
        'trænsˈleɪʃən a.2',
    ],
    'senses' => [
        [
            'definition' => 'definition a.1',
            'translations' => [
                'translation a.1.1',
                'trænsˈleɪʃən a.1.2',
            ],
        ],
        [
            'definition' => 'ˌdɛfɪˈnɪʃən a.2',
            'translations' => [
                'translation a.2.1',
                'trænsˈleɪʃən a.2.2',
            ],
        ]
    ],
];
