<?php

declare(strict_types=1);

return [
    'headwords' => [
        'headword 1',
        'ˈhɛdˌwɜːd 2',
    ],
    'homograph' => 3,
    'pronunciations' => [
        'pronunciation 1',
        'prəˌnʌnsɪˈeɪʃən 2',
    ],
    'categories' => [
        'category 1',
        'ˈkætɪɡərɪ 2',
    ],
    'forms' => [
        'form label 1' => [
            'form 1.1',
            'fɔːm 1.2',
        ],
        'form label 2' => [
            'fɔːm ˈleɪ bəl 2.1' => [
                'form 2.1.1',
                'fɔːm 2.1.2',
            ],
        ],
    ],
    'varieties' => [
        'variety 1',
        'variety 2',
    ],
    'registers' => [
        'register 1',
        'register 2',
    ],
    'domains' => [
        'domain 1',
        'domain 2',
    ],
    'definition' => 'definition',
    'translations' => [
        'translation 1',
        'trænsˈleɪʃən 2',
    ],
    'synonyms' => [
        'synonym 1',
        'synonym 2',
    ],
    'antonyms' => [
        'antonym 1',
        'antonym 2',
    ],
    'collocations' => [
        [
            'headwords' => [
                'headword a.1',
                'ˈhɛdˌwɜːd a.2',
            ],
            'pronunciations' => [
                'pronunciation a.1',
                'prəˌnʌnsɪˈeɪʃən a.2',
            ],
            'definition' => 'definition a',
            'translations' => [
                'translation a.1',
                'trænsˈleɪʃən a.2',
            ],
            'senses' => [
                [
                    'definition' => 'definition a.1',
                    'translations' => [
                        'translation a.1.1',
                        'trænsˈleɪʃən a.1.2',
                    ],
                ],
                [
                    'definition' => 'ˌdɛfɪˈnɪʃən a.2',
                    'translations' => [
                        'translation a.2.1',
                        'trænsˈleɪʃən a.2.2',
                    ],
                ],
            ]
        ],
        [
            'headwords' => [
                'headword b.1',
                'ˈhɛdˌwɜːd b.2',
            ],
            'pronunciations' => [
                'pronunciation b.1',
                'prəˌnʌnsɪˈeɪʃən b.2',
            ],
            'definition' => 'ˌdɛfɪˈnɪʃən b',
            'translations' => [
                'translation b.1',
                'trænsˈleɪʃən b.2',
            ],
        ],
    ],
    'senses' => [
        [
            'context' => 'ˈkɒntekst 1',
            'definition' => 'definition 1',
            'translations' => [
                'translation 1.1',
                'trænsˈleɪʃən 1.2',
            ],
            'synonyms' => [
                'synonym 1.1',
                'synonym 1.2',
            ],
            'antonyms' => [
                'antonym 1.1',
                'antonym 1.2',
            ],
            'collocations' => [
                [
                    'headwords' => [
                        'headword 1.a.1',
                        'ˈhɛdˌwɜːd 1.a.2',
                    ],
                    'pronunciations' => [
                        'pronunciation 1.a.1',
                        'prəˌnʌnsɪˈeɪʃən 1.a.2',
                    ],
                    'definition' => 'definition 1.a',
                    'translations' => [
                        'translation 1.a.1',
                        'trænsˈleɪʃən 1.a.2',
                    ],
                    'senses' => [
                        [
                            'definition' => 'definition 1.a.1',
                            'translations' => [
                                'translation 1.a.1.1',
                                'trænsˈleɪʃən 1.a.1.2',
                            ],
                        ],
                        [
                            'definition' => 'ˌdɛfɪˈnɪʃən 1.a.2',
                            'translations' => [
                                'translation 1.a.2.1',
                                'trænsˈleɪʃən 1.a.2.2',
                            ],
                        ],
                    ]
                ],
                [
                    'headwords' => [
                        'headword 1.b.1',
                        'ˈhɛdˌwɜːd 1.b.2',
                    ],
                    'pronunciations' => [
                        'pronunciation 1.b.1',
                        'prəˌnʌnsɪˈeɪʃən 1.b.2',
                    ],
                    'definition' => 'ˌdɛfɪˈnɪʃən 1.b',
                    'translations' => [
                        'translation 1.b.1',
                        'trænsˈleɪʃən 1.b.2',
                    ],
                ],
            ],
        ],
        [
            'definition' => 'ˌdɛfɪˈnɪʃən 2',
            'translations' => [
                'translation 2.1',
                'trænsˈleɪʃən 2.2',
            ],
        ],
    ],
    'derivatives' => [
        'derivative 1',
        'derivative 2',
    ],
    'etymons' => [
        [
            'language' => 'language 1',
            'lemma' => 'lemma 1',
            'gloss' => 'gloss 1',
        ],
        [
            'lemma' => 'lemma 2',
        ]
    ],
    'sound_changes' => [
        'sound change 1',
        'saʊnd tʃeɪndʒ 2',
    ],
    'cognates' => [
        [
            'language' => 'language 1',
            'lemma' => 'lemma 1',
            'gloss' => 'gloss 1',
        ],
        [
            'language' => 'language 2',
            'lemma' => 'lemma 2',
        ]
    ]
];
