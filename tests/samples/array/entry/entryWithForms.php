<?php

declare(strict_types=1);

return [
    'headwords' => [
        'headword 1'
    ],
    'forms' => [
        'form label 1' => 'form 1',
        'form label 2' => [
            'form label 2.1' => 'form 2.1'
        ]
    ],
    'translations' => [
        'translation 1'
    ],
];
