<?php

declare(strict_types=1);

return [
    'headwords' => [
        'headword 1',
        'hɛdˌwɜːd 2',
        'headword 3',
        'ˈhɛdˌwɜːd 4',
        'headword 5',
        'ˈhɛdˌwɜːd 6',
        'headword 7',
        'ˈhɛdˌwɜːd 8',
        'headword 9',
        'ˈhɛdˌwɜːd 10',
    ],

    'homograph' => 3,

    'pronunciations' => [
        'pronunciation 1',
        'prəˌnʌnsɪˈeɪʃən 2',
        'pronunciation 3',
        'prəˌnʌnsɪˈeɪʃən 4',
        'pronunciation 5',
        'prəˌnʌnsɪˈeɪʃən 6',
        'pronunciation 7',
        'prəˌnʌnsɪˈeɪʃən 8',
    ],

    'categories' => [
        'category 1',
        'ˈkætɪɡərɪ 2',
        'category 3',
        'ˈkætɪɡərɪ 4',
        'category 5',
        'ˈkætɪɡərɪ 6',
        'category 7',
        'ˈkætɪɡərɪ 8',
    ],

    // no merging
    'forms' => [
        'form label 1' => ['form 1.1', 'fɔːm 1.2'],
        'form label 2' => ['form 2.1', 'fɔːm 2.2'],
        'form label 3' => ['form 3.1', 'fɔːm 3.2'],
    ],

    'definition' => 'definition 0',

    'translations' => [
        'translation 1',
        'trænsˈleɪʃən 2',
        'translation 3',
        'trænsˈleɪʃən 4',
        'translation 5',
        'trænsˈleɪʃən 6',
        'translation 7',
        'trænsˈleɪʃən 8',
        'translation 9',
        'trænsˈleɪʃən 10',
        'translation 11',
        'trænsˈleɪʃən 12',
    ],

    'synonyms' => [
        'synonym 1',
        'synonym 2',
        'synonym 3',
        'synonym 4',
        'synonym 5',
        'synonym 6',
    ],

    'antonyms' => [
        'antonym 1',
        'antonym 2',
        'antonym 3',
        'antonym 4',
        'antonym 5',
        'antonym 6',
    ],

    'collocations' => [
        ['headwords' => ['headword 0.a.1'], 'translations' => ['translation 0.a.1']],
        ['headwords' => ['headword 0.b.1'], 'translations' => ['translation 0.b.1']],
        ['headwords' => ['headword 0.c.1'], 'translations' => ['translation 0.c.1']],
        ['headwords' => ['headword 0.d.1'], 'translations' => ['translation 0.d.1']],
        ['headwords' => ['headword 0.e.1'], 'translations' => ['translation 0.e.1']],
    ],

    'senses' => [
        ['definition' => 'definition 1', 'translations' => ['translation 1.1']],
        ['definition' => 'definition 2', 'translations' => ['translation 2.1']],
        ['definition' => 'definition 3', 'translations' => ['translation 3.1']],
    ],

    'derivatives' => [
        'derivative 1',
        'derivative 2',
        'derivative 3',
        'derivative 4',
        'derivative 5',
        'derivative 6',
        'derivative 7',
        'derivative 8',
    ],

    'etymons' => [
        ['language' => 'language 1', 'lemma' => 'lemma 1', 'gloss' => 'gloss 1'],
        ['language' => 'language 2', 'lemma' => 'lemma 2', 'gloss' => 'gloss 2'],
        ['language' => 'language 3', 'lemma' => 'lemma 3', 'gloss' => 'gloss 3'],
        ['language' => 'language 4', 'lemma' => 'lemma 4', 'gloss' => 'gloss 4'],
    ],

    'sound_changes' => [
        'sound change 1',
        'sound change 2',
        'sound change 3',
        'sound change 4',
        'sound change 5',
        'sound change 6',
        'sound change 7',
        'sound change 8',
        'sound change 9',
        'sound change 10',
        'sound change 11',
        'sound change 12',
    ],

    'cognates' => [
        ['language' => 'language 1', 'lemma' => 'lemma 1', 'gloss' => 'gloss 1'],
        ['language' => 'language 2', 'lemma' => 'lemma 2', 'gloss' => 'gloss 2'],
        ['language' => 'language 3', 'lemma' => 'lemma 3', 'gloss' => 'gloss 3'],
        ['language' => 'language 4', 'lemma' => 'lemma 4', 'gloss' => 'gloss 4'],
        ['language' => 'language 5', 'lemma' => 'lemma 5', 'gloss' => 'gloss 5'],
    ],
];
