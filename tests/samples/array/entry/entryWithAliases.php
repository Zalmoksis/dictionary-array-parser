<?php

declare(strict_types=1);

return [
    'headwords' => ['headword 1', 'hɛdˌwɜːd 2'],
    'headword' => ['headword 3', 'ˈhɛdˌwɜːd 4'],
    'head' => ['headword 5', 'ˈhɛdˌwɜːd 6'],
    'hw' => ['headword 7', 'ˈhɛdˌwɜːd 8'],
    'h' => ['headword 9', 'ˈhɛdˌwɜːd 10'],

    'hom' => 3,

    'pronunciations' => ['pronunciation 1', 'prəˌnʌnsɪˈeɪʃən 2'],
    'pronunciation' => ['pronunciation 3', 'prəˌnʌnsɪˈeɪʃən 4'],
    'pron' => ['pronunciation 5', 'prəˌnʌnsɪˈeɪʃən 6'],
    'p' => ['pronunciation 7', 'prəˌnʌnsɪˈeɪʃən 8'],

    'categories' => ['category 1', 'ˈkætɪɡərɪ 2'],
    'category' => ['category 3', 'ˈkætɪɡərɪ 4'],
    'cat' => ['category 5', 'ˈkætɪɡərɪ 6'],
    'c' => ['category 7', 'ˈkætɪɡərɪ 8'],

    'forms' => ['form label 1' => ['form 1.1', 'fɔːm 1.2']],
    'form' => ['form label 2' => ['form 2.1', 'fɔːm 2.2']],
    'f' => ['form label 3' => ['form 3.1', 'fɔːm 3.2']],

    'def' => 'definition 0',

    'translations' => ['translation 1', 'trænsˈleɪʃən 2'],
    'translation' => ['translation 3', 'trænsˈleɪʃən 4'],
    'transl' => ['translation 5', 'trænsˈleɪʃən 6'],
    'trans' => ['translation 7', 'trænsˈleɪʃən 8'],
    'tr' => ['translation 9', 'trænsˈleɪʃən 10'],
    't' => ['translation 11', 'trænsˈleɪʃən 12'],

    'synonyms' => ['synonym 1', 'synonym 2'],
    'synonym' => ['synonym 3', 'synonym 4'],
    'syn' => ['synonym 5', 'synonym 6'],

    'antonyms' => ['antonym 1', 'antonym 2'],
    'antonym' => ['antonym 3', 'antonym 4'],
    'ant' => ['antonym 5', 'antonym 6'],

    'collocations' => [['headwords' => ['headword 0.a.1'], 'translations' => ['translation 0.a.1']]],
    'collocation' => [['headword' => ['headword 0.b.1'], 'translation' => ['translation 0.b.1']]],
    'colloc' => [['head' => ['headword 0.c.1'], 'trans' => ['translation 0.c.1']]],
    'col' => [['hw' => ['headword 0.d.1'], 'tr' => ['translation 0.d.1']]],
    'cl' => [['h' => ['headword 0.e.1'], 't' => ['translation 0.e.1']]],

    'senses' => [['definition' => 'definition 1', 'translations' => ['translation 1.1']]],
    'sense' => [['definition' => 'definition 2', 'translation' => ['translation 2.1']]],
    's' => [['d' => 'definition 3', 't' => ['translation 3.1']]],

    'derivatives' => ['derivative 1', 'derivative 2'],
    'derivative' => ['derivative 3', 'derivative 4'],
    'deriv' => ['derivative 5', 'derivative 6'],
    'der' => ['derivative 7', 'derivative 8'],

    'etymons' => [['language' => 'language 1', 'lemma' => 'lemma 1', 'gloss' => 'gloss 1']],
    'etymon' => [['language' => 'language 2', 'lemma' => 'lemma 2', 'gloss' => 'gloss 2']],
    'etym' => [['lang' => 'language 3', 'lem' => 'lemma 3', 'gloss' => 'gloss 3']],
    'et' => [['ln' => 'language 4', 'lm' => 'lemma 4', 'gl' => 'gloss 4']],

    'sound_changes' => ['sound change 1', 'sound change 2'],
    'sound_change' => ['sound change 3', 'sound change 4'],
    'change' => ['sound change 5', 'sound change 6'],
    'chan' => ['sound change 7', 'sound change 8'],
    'sch' => ['sound change 9', 'sound change 10'],
    'sc' => ['sound change 11', 'sound change 12'],

    'cognates' => [['language' => 'language 1', 'lemma' => 'lemma 1', 'gloss' => 'gloss 1']],
    'cognate' => [['language' => 'language 2', 'lemma' => 'lemma 2', 'gloss' => 'gloss 2']],
    'cogn' => [['lang' => 'language 3', 'lem' => 'lemma 3', 'gloss' => 'gloss 3']],
    'cgn' => [['lng' => 'language 4', 'lm' => 'lemma 4', 'gl' => 'gloss 4']],
    'cg' => [['ln' => 'language 5', 'lm' => 'lemma 5', 'g' => 'gloss 5']],
];
