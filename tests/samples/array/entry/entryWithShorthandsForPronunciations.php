<?php

declare(strict_types=1);

return [
    'headwords' => [
        'ˈhɛdˌwɜːd',
    ],
    'pronunciations' => 'prəˌnʌnsɪˈeɪʃən',
    'categories' => [
        'ˈkætɪɡərɪ',
    ],
    'forms' => [
        'form label 1' => [
            'fɔːm 1',
        ],
        'form label 2' => [
            'fɔːm ˈleɪ bəl 2.1' => [
                'fɔːm 2.1',
            ],
        ],
    ],
    'varieties' => [
        'variety',
    ],
    'registers' => [
        'register',
    ],
    'domains' => [
        'domain',
    ],
    'translations' => [
        'trænsˈleɪʃən',
    ],
    'synonyms' => [
        'ˈsɪnəˌnɪm',
    ],
    'antonyms' => [
        'ˈæntənɪm',
    ],
    'senses' => [
        [
            'translations' => [
                'trænsˈleɪʃən 1',
            ],
        ],
        [
            'translations' => [
                'trænsˈleɪʃən 2',
            ],
        ],
    ],
    'derivatives' => [
        'derivative',
    ],
    'sound_changes' => [
        'saʊnd tʃeɪndʒ',
    ],
];
