<?php

declare(strict_types=1);

return [
    'varieties' => [
        'variety 1',
        'variety 2',
    ],
    'registers' => [
        'register 1',
        'register 2',
    ],
    'domains' => [
        'domain 1',
        'domain 2',
    ],
    'context' => 'ˈkɒntekst',
    'definition' => 'ˌdɛfɪˈnɪʃən',
    'translations' => [
        'translation 1',
        'trænsˈleɪʃən 2',
    ],
    'synonyms' => [
        'synonym 1',
        'synonym 2',
    ],
    'antonyms' => [
        'antonym 1',
        'antonym 2',
    ],
    'collocations' => [
        [
            'headwords' => [
                'headword a.1',
                'ˈhɛdˌwɜːd a.2',
            ],
            'pronunciations' => [
                'pronunciation a.1',
                'prəˌnʌnsɪˈeɪʃən a.2',
            ],
            'definition' => 'definition a',
            'translations' => [
                'translation a.1',
                'trænsˈleɪʃən a.2',
            ],
        ],
        [
            'headwords' => [
                'headword b.1',
                'ˈhɛdˌwɜːd b.2',
            ],
            'definition' => 'ˌdɛfɪˈnɪʃən b',
            'translations' => [
                'translation b.1',
                'trænsˈleɪʃən b.2',
            ],
        ],
    ],
    'senses' => [
        [
            'definition' => 'definition 1',
            'translations' => [
                'translation 1.1',
                'trænsˈleɪʃən 1.2',
            ],
        ],
        [
            'definition' => 'ˌdɛfɪˈnɪʃən 2',
            'translations' => [
                'translation 2.1',
                'trænsˈleɪʃən 2.2',
            ],
        ],
    ],
];
